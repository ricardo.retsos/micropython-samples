# Usando Pytrack

##### Autor : Roberto Colistete Jr. (roberto.colistete arroba gmail.com)
##### Última atualização : 15/06/2020.

Obs.: em Maio de 2020 foi anunciada a [Pytrack v2X](https://pycom.io/product/pytrack-2-0-x/) com 4 melhores. Esse tutorial cobre Pytrack v1.

Placa [PyTrack v1](https://pycom.io/product/pytrack/) da Pycom usa o excelente módulo [Quectel GNSS L76](https://www.quectel.com/product/l76.htm) 
compatível com satélites GPS, GLONASS, BeiDou, Galileo e QZSS. Tem também :

* antena GPS embutida na placa, porém sem conector para antena GPS externa;
* acelerômetro 3D de 12 bits, que inclusive pode ser usado para acordar de deepsleep da Pytrack quando ocorrer aceleração acima de certo patamar;
* porta microUSB para alimentação de energia e comunicação USB serial;
* leitor/gravador de microSD;
* conector JST PH 2.0 para bateria (consegue carregar com 450 mA), com LED indicando quando está carregando;
* user push button;
* "External IO Header" para 10 conexões que podem ter pinos ou fio soldados (porém há pouco espaço). 

A placa PyTrack com os firmware mais recentes permite sim atualizar firmware MicroPython das placas xxPy (LoPy4, WiPy, FiPy, etc) da Pycom, sem 
necessidade de jumper. Esse recurso demorou a aparecer, não sendo disponível nos primeiros firmwares da Pytrack.

Em testes em sala a 3m de janela, consegui que Pytrack recebesse sinais de satélites GPS sem problemas, logo a sensibilidade do GNSS L76 e da antena 
embutida aparenta ser boa.

<img src="https://pycom.io/wp-content/uploads/2018/08/pytrackSideJune.png" alt="Pytrack da Pycom" width="600"/>


## Instalação de firmware da placa PyTrack

Pytrack usa microcontrolador PIC que tem um firmware independente do firmware MicroPython das placas xxPy (LoPy4, WiPy, FiPy, etc) da Pycom.

Não dá para ler e saber qual versão de firmware que já instalado na placa Pytrack. Logo é importante ao menos uma vez instalar versão atual do firmware 
Pycom para Pytrack.

Seguir [documentação da Pycom para instalação de firwmare em Pytrack Pysense](https://docs.pycom.io/pytrackpysense/installation/firmware/) :

1. fazer download do "Pytrack DFU" (última versão é 0.0.8);

2. Instalar 'dfu-util' no computador. No Linux com Debian e derivados, usando terminal :  
`$ sudo apt-get install dfu-util`

3. Pode instalar o firmware da placa PyTrack sem ter xxPy4 (LoPy4, WiPy3, FiPy, etc), isso é opcional, alguns até recomendam;

4. Seguir à risca, normalmente funciona após tentar algumas vezes por causa dos intervalos de tempo :

> To enter update mode follow these steps:  
> Unplug the device  
> Press the button and keep it held (on the Expansion Board the S1 button)  
> Plug in the USB cable to the host computer and wait 1 second before releasing the button  
> After this you will have approximately 7 seconds to run the DFU-util tool

5. Ou seja, rodar 'dfu-util' poucos segundos após soltar o botão da PyTrack, usando terminal no computador com Linux :  
```
$ sudo dfu-util -D pytrack_0.0.8.dfu
dfu-util 0.9

Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
Copyright 2010-2016 Tormod Volden and Stefan Schmidt
This program is Free Software and has ABSOLUTELY NO WARRANTY
Please report bugs to http://sourceforge.net/p/dfu-util/tickets/

Match vendor ID from file: 04d8
Match product ID from file: f014
Opening DFU capable USB device...
ID 04d8:f014
Run-time device DFU version 0100
Claiming USB DFU Runtime Interface...
Determining device status: state = dfuIDLE, status = 0
dfu-util: WARNING: Runtime device already in DFU state ?!?
Claiming USB DFU Interface...
Setting Alternate Setting #0 ...
Determining device status: state = dfuIDLE, status = 0
dfuIDLE, continuing
DFU mode device DFU version 0100
Device returned transfer size 64
Copying data from PC to DFU device
Download	[=========================] 100%        16384 bytes
Download done.
state(2) = dfuIDLE, status(0) = No error condition is present
Done!
```


## Instalação de módulos MicroPython da Pycom

Para instalação de módulos MicroPython da Pycom, seguir [documentação da Pycom para Pytrack, Pysense e 
Pyscan](https://docs.pycom.io/pytrackpysense/installation/libraries/) :

* fazer download de ["MicroPython libraries and examples" da Pycom](https://github.com/pycom/pycom-libraries);
* colocar em '/flash/lib' (memória flash interna) do xxPy (LoPy4, WiPy3, FiPy, etc) os [módulos L76GNSS.py, LIS2HH12.py, 
pytrack.py](https://github.com/pycom/pycom-libraries/tree/master/pytrack/lib) e [pycoproc.py](https://github.com/pycom/pycom-libraries/tree/master/lib/pycoproc).


## Documentação de referência sobre Pytrack

* [Pytrack API Reference sobre acelerômetro e GPS](https://docs.pycom.io/pytrackpysense/apireference/pytrack/);
* [Pytrack API Reference sobre sleep, wakeup, etc](https://docs.pycom.io/pytrackpysense/apireference/sleep/).


## Exemplos da Pycom

* [para ler as coordenadas (latitude e longitude do Pytrack](https://docs.pycom.io/tutorials/pytrack/), porém não é muito didático 
pois assume conexão WiFi para sincronizar o RTC com servidor NTP. Tal exemplo também está em ["MicroPython libraries and examples" da 
Pycom sobre Pytrack](https://github.com/pycom/pycom-libraries/tree/master/pytrack);
* [de leitura do acelerômetro LIS2HH12](https://docs.pycom.io/tutorials/pysense/). Tal exemplo também está em ["MicroPython libraries and examples" da 
Pycom, sobre acelerômetro de Pytrack/Pysense](https://github.com/pycom/pycom-libraries/tree/master/examples/pytrack_pysense_accelerometer);
* [de uso de acelerômetro da Pytrack para acordar de deepsleep](https://github.com/pycom/pycom-libraries/tree/master/examples/accelerometer_wake).


## Uso interativo de Pytrack com módulos Pycom

Teste de acelerômetro. O módulo LIS2HH12 tem várias funções e recursos não documentados. Pratique no terminal/REPL :

```python
from LIS2HH12 import LIS2HH12
acc = LIS2HH12()
acc.acceleration()
acc.roll()
acc.pitch()
acc.x
acc.y
acc.z
```

Teste de GPS. O módulo L76GNSS é bem limitado, só retorna longitude e latitude. 
```python
from L76GNSS import L76GNSS
from pytrack import Pytrack
py = Pytrack()
l76 = L76GNSS(py, timeout=30)
l76.coordinates()   # Na casa em Guaçuí : (-20.779, -41.66776)
```


## Uso de módulos extras para obter mais informações GNSS da Pytrack

Para uso do GNSS da placa PyTrack com altitude, direção, velocidade, hora GPS, número de satélites conectados, etc, tem que usar 
módulos MicroPython extras, vide final do [Tutorial da Pycom sobre Pytrack](https://docs.pycom.io/tutorials/pytrack/).

1. micropyGPS, vide GitHub](https://github.com/inmcm/micropyGPS) e [tópico "Pytrack GPS library" no fórum 
Pycom](https://forum.pycom.io/topic/3870/pytrack-gps-library).

2. "Alternative L76GNSS module", vide no GitHub [v4](https://github.com/andrethemac/L76GLNSV4/) e [v5](https://github.com/andrethemac/L76GLNSV5), 
bem como [tópico "Pytrack GPS API" no fórum Pycom](https://forum.pycom.io/topic/1626/pytrack-gps-api/).


Exemplo usando módulo MicroPython L76GLNSV4 (basta copiar "L76GNSV4.py" para "/flash/lib/"). Use terminal/REPL :

```python
from pytrack import Pytrack
from L76GNSV4 import L76GNSS

py = Pytrack()
L76 = L76GNSS(pytrack=py)
L76.setAlwaysOn()

L76.gps_message('GSV', debug=True)

print("gga")
L76.gps_message('GSV', debug=True)    # info about sattelites in view at this moment even without the gps being fixed
L76.gps_message('GSV')                # info about sattelites in view at this moment even without the gps being fixed
L76.gps_message('GGA')['NumberOfSV']  # returns the number of sattelites in view at this moment even without the gps being fixed
L76.gps_message('GGA')                # essentials fix and accuracy data
L76.gps_message('GLL')                # GLL sentence (geolocation)
L76.gps_message('RMC')	              # required minimum position dat
L76.gps_message('VTG')                # track and ground speed
L76.gps_message('GSA')                # fix state, the sattelites used and DOP info
L76.get_fix()
L76.fixed()
L76.coordinates()           # {'latitude': -20.77906, 'longitude': -41.66759, 'ttf': 0}
L76.get_location()          # {'HDOP': '1.10', 'altitude': '606.5', 'longitude': -41.66759, 'ttf': 0, 'latitude': -20.77906}
L76.getUTCDateTime()        # '2019-10-18T18:08:37+00:00'
L76.getUTCDateTimeTuple()   # (2019, 10, 18, 18, 3, 16) 
L76.get_speed()             # {'speed': '0.00', 'COG': '120.88'}. Retorna velocidade e direção. L76 usa cálculo Doppler do sinal GPS, precisão de 0,1 m/s ! 
```

[Teoria sobre HDOP x erro em distância](https://web.archive.org/web/20140222000521/http://www.crowtracker.com/hdop-gps-position-errors/).

