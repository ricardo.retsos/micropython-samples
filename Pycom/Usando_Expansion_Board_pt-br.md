# Usando Expansion Board

##### Autor : [Roberto Colistete Jr.](https://gitlab.com/rcolistete)

##### Última atualização : 27/06/2020.

A placa Expansion Board da Pycom é compatível com todas as placas xxPy (WiPy 1, WiPy 2, WiPy 3, LoPy v1, SiPy, LoPy4, FiPy e GPy)
e possibilita várias conexões, tornando desenvolvimento muito mais fácil. Todas as placas Expansion Board v2.x/v3.x têm :

* conectores fêmea expondo todos os 28 pinos dos xxPy (LoPy4, etc).
* porta microUSB para alimentação de energia e comunicação USB serial;
* leitor/gravador de microSD;
* conector JST PH 2.0 e [circuito carregador BQ24040](https://www.ti.com/product/BQ24040) de bateria Li-Ion/LiPo (carrega com 100 
ou 450 mA), com proteção contra inversão de tensão da bateria via [circuito integrado TPS2115A](https://www.ti.com/product/TPS2115A);
se houver alimentação via USB;
* LED 'USB' indicando conexão USB;
* LED 'CHG' indicando 'charging' (bateria carregando) 
* 'LED' configurável pelo usuário;
* botão de usuário, 'user push button';
* divisor de tensão, com 2 resistores, conectado ao pinoVBATT/G3/P16 para ler via ADC a tensão da bateria sem estourar o limite de 3,3V;
* 7 mini-jumpers p/ configuração, vide [seção abaixo](#configuração-dos-mini-jumpers);

**ATENÇÃO :**
- **xxPy deve ser conectado à placa Expansion Board tal que o LED RGB fique perto do conector microUSB, bem como todos os pinos inseridos;**
- **checar (com multímetro) a polaridade da bateria antes de conectar, pois pode queimar a Expansion Board** se a polaridade estiver 
invertida e tiver alimentação USB;
- **ao usar FiPy ou GPy com Expansion Board, retire os mini-jumpers RTS e CTS**, pois conflitam com a operação do modem LTE, vide 
tutoriais de [CAT-M1](https://docs.pycom.io/tutorials/lte/cat-m1/) e de [NB-IoT](https://docs.pycom.io/tutorials/lte/nb-iot/).

As placas Expansion Board da Pycom evoluíram em várias versões : v?, v2.0, v2.1, v2.1A, v3.0 e v3.1.


## 1. Documentação :


1. [site oficial da Pycom, Expansion Board v2.x](https://docs.pycom.io/datasheets/boards/expansion2/), que mostra bem o 
significado dos mini-jumpers;
2. [manual oficial da Pycom, da Expansion Board v2.0](https://github.com/wipy/wipy/blob/master/docs/User_manual_exp_board.pdf), embora 
antigo é altamente recomendado por ter vários detalhes não disponíveis no site Pycom da documentação oficial, como diagrama de blocos, 
descrição dos mini-jumpers, exemplos de códigos-fonte, etc;
3. [site oficial da Pycom, Expansion Board v3.x](https://docs.pycom.io/datasheets/boards/expansion3/), onde a figura do pinout 
mostra resistores errados, a versão PDF está correta com 2 resistores de 1 Mohm.
4. [tutorial "Pycom Expansion Board 2.0 Overview"](https://core-electronics.com.au/tutorials/pycom-expansion-board-2-0-overview.html) 
da Core Electronics;
5. [tutorial "Pycom Expansion Board - Getting Started"](https://core-electronics.com.au/tutorials/pycom-expansion-board-2-0-getting-started.html) 
da Core Electronics.

A descrição dos conectores fêmea nas placas Expansion Board v2.x e v3.0 usa notação 'Gn', enquanto que a Expansion Board v3.1 adotou a notação 
'Pn'. Vide relação entre as notações 'Gn' e 'Pn' :
- ['pins.csv' no código-fonte oficial do Pycom MicroPython](https://github.com/pycom/pycom-micropython-sigfox/blob/Dev/esp32/boards/WIPY/pins.csv);
- [tópico 'Mapping of G pins and P pin on Exp. Board v3' no fórum Pycom](https://forum.pycom.io/topic/3919/mapping-of-g-pins-and-p-pin-on-exp-board-v3?_=1592400643509).


## 2. Expansion Board v2.x

A placa Expansion Board v2.x (lançada em 2015) tem :
- circuito integrado USB serial FTDI, visto no Linux como '/dev/ttyUSB0';
- modo bootloader manual, sendo necessário colocar fio/jumper entre pinos G23/P2 e GND antes de ligar a alimentação USB para 
atualizar firmware MicroPython dos xxPy (LoPy4, etc).
- [modo safe boot](https://docs.pycom.io/gettingstarted/programming/safeboot/) sendo necessário colocar fio/jumper entre pinos 
G28/P12 e 3V3 antes de ligar o cabo USB;
- divisor de tensão com resistores de 56 kOhm (entre GND e pino VBATT/G3/P16) e 115 kOhm (entre bateria e pino VBATT/G3/P16) para 
ler a tensão da bateria Li-Ion/Li-Po (3,6-3,7 V nominais), normalmente entre 3,0 V (bem descarregada) a 4,2 V (bem carregada), 
sem estourar o limite de 3,3V do ADC do ESP32/xxPy;
- consumo de energia mediano em modo deep sleep, mínimo de 156 uA com WiPy3 e bateria fornecendo 3,61-3,66 V no conector JST PH 2.0.

<img src="https://cdn-shop.adafruit.com/1200x900/3344-02.jpg" title="Expansion Board v2.1A" alt="Expansion Board v2.1A" width="600"/>


## 3. Expansion Board v3.x

A placa Expansion Board v3.0 (lançada no início de 2018) tem :
- tem microcontrolador PIC (de baixo consumo de energia), com firmware próprio, funcionando como circuito USB serial (visto no Linux 
como '/dev/ttyACM0'), gerenciador de bootloader e de modo de economia de energia (deepsleep);
- modo bootloader automático, permitindo atualizar firmware MicroPython dos xxPy (LoPy4, etc) sem necessidade de jumper entre 
G23/P2 e GND;
- botão "Safe Boot" para entrar em [modo safe boot](https://docs.pycom.io/gettingstarted/programming/safeboot/) facilment, sem precisar 
conectar pinos G28/P12 e 3V3;
- divisor de tensão com resistores de 1 MOhm (entre GND e pino VBATT/G3/P16) e 1 MOhm (entre bateria e pino VBATT/G3/P16) para ler a tensão 
da bateria Li-Ion/Li-Po (3,6-3,7 V nominais), normmalmente entre 3,0 V (bem descarregada) a 4,2 V (bem carregada), sem estourar o limite 
de 3,3V do ADC do ESP32/xxPy;
- consumo de energia mediano em modo deep sleep, mínimo de 233 uA com WiPy3 e bateria fornecendo 3,61-3,66 V no conector JST PH 2.0;
- [**bug que impede de dar boot ao usar alimentação por bateria**](https://forum.pycom.io/topic/3436/expansion-board-3-0-battery-power-not-enough-juice).
**Solução é conectar um fio/jumper entre os pinos G15/P8 e 3V3**, mas se for usar a unidade de uSD da Expansion Board então deve ser colocado resistor 
de 10 kOhm entre os pinos G15/P8 e 3V3. Vide aviso na documentação oficial,
["To use the battery, pull P8/G15 high (connect to 3v3). If you want to use the SD card as well, use a 10k pull-up"](https://docs.pycom.io/datasheets/boards/expansion3/)

<img src="https://cdn.sparkfun.com//assets/parts/1/2/8/7/4/14675-LoPy_Expansion_Board_2.0-04.jpg" title="Expansion Board v3.0" 
alt="Expansion Board v3.0" width="600"/>

Expansion Board v3.1 (lançada no início de 2019) muda em relação a v3.0 :
- tem um quadrado branco com QRcode na parte superior ao lado do microSD, útil para diferenciar da Expansion Board v3.0 sem olhar a 
parte inferior;
- a descrição dos conectores fêmea usando notação 'Pn' ao invés de 'Gn';
- **baixo consumo de energia em modo deep sleep, mínimo de 34 mA com WiPy3 e bateria fornecendo 3,61-3,66 V no conector JST PH 2.0**, 
semelhante aos [33 uA prometidos pela Pycom](https://forum.pycom.io/topic/4336/new-v3-1-expansion-board-firmware-v0-0-11);
- **(característica ou bug ?) maior consumo de energia no modo ativo (122-128 mW a mais) e light sleep (113 mW a mais) ao usar** 
**mini-jumpers TX/RX (USB serial) e alimentação por bateria**;
- **correção do bug que impedia de dar boot ao usar alimentação por bateria, não mais precisando conectar um fio/jumper** (ou resistor de 
10 kOhm) entre os pinos G15/P8 e 3V3.

<img src="https://pycom.io/wp-content/uploads/2020/03/Website-Product-Shots-ExpB-front-WiPy.png" title="Expansion Board v3.1 com WiPy 3" 
alt="Expansion Board v3.1 com WiPy 3" width="700"/>


Nos códigos-fonte de testes :
- digite tecla 'Tab' após '.' em final de linha para ver a lista de atributos do objeto;
- saídas/resultados estão indentados com 1 tabulação a mais;
- foi usado Pycom MicroPython oficial v1.20.2.rc7.

### 3.1. Instalação de firmware na Expansion Board v3.x

Expansion Board v3.x usa microcontrolador PIC que tem um firmware independente do firmware MicroPython das placas xxPy 
(LoPy4, WiPy, FiPy, etc) da Pycom.

Não dá para ler e saber qual versão de firmware que já instalado na placa Expansion Board v3.x. Logo é importante ao 
menos uma vez instalar versão atual do firmware Pycom para Expansion Board v3.x e colocar uma etiqueta na placa com 
a versão do firmware.

Seguir [documentação da Pycom para instalação de firwmare em Expansion Board v3.x, Pytrack e Pysense](https://docs.pycom.io/pytrackpysense/installation/firmware/) :

1. fazer download do firmware "Expansion Board DFU v3.0" (última versão é 'expansion3_0.0.9.dfu') p/ Expansion Board v3.0 
e do firmware "Expansion Board DFU v3.1" (última versão é 'expansion31_0.0.11.dfu') p/ Expansion Board v3.1;

2. Instalar 'dfu-util' no computador. No Linux com Debian e derivados, usando terminal :  
`$ sudo apt-get install dfu-util`

3. Pode instalar o firmware da placa Expansion Board v3.x sem ter xxPy (LoPy4, WiPy3, FiPy, etc), aliás, isso é recomendado 
(menos hardware para dar problema...);

4. Seguir à risca, normalmente funciona após tentar algumas vezes por causa dos intervalos de tempo :
   - desconecte o cabo USB da placa Expansion Board v3.x ao computador;
   - pressione o botão 'S1' e mantenha pressionado enquanto conecta o cabo USB entre o computador e a placa Expansion Board v3.x;
   - espere 1 segundo ou pouco mais antes de soltar o botão 'S1';
   - você tem aproximadamente 7 segundos para rodar o comando dfu-util usando terminal no computador com Linux, a dica é já deixar 
     toda a linha digitada, faltando só teclar "Enter";

5. na Expansion Board v3.0 :
```
$ sudo dfu-util -D expansion3_0.0.9.dfu
    dfu-util 0.9
    
    Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
    Copyright 2010-2016 Tormod Volden and Stefan Schmidt
    This program is Free Software and has ABSOLUTELY NO WARRANTY
    Please report bugs to http://sourceforge.net/p/dfu-util/tickets/
    
    Match vendor ID from file: 04d8
    Match product ID from file: ef99
    Opening DFU capable USB device...
    ID 04d8:ef99
    Run-time device DFU version 0100
    Claiming USB DFU Runtime Interface...
    Determining device status: state = dfuIDLE, status = 0
    dfu-util: WARNING: Runtime device already in DFU state ?!?
    Claiming USB DFU Interface...
    Setting Alternate Setting #0 ...
    Determining device status: state = dfuIDLE, status = 0
    dfuIDLE, continuing
    DFU mode device DFU version 0100
    Device returned transfer size 64
    Copying data from PC to DFU device
    Download	[=========================] 100%        16384 bytes
    Download done.
    state(2) = dfuIDLE, status(0) = No error condition is present
    Done!
```

6. na Expansion Board v3.1 :
```
$ sudo dfu-util -D expansion31_0.0.11.dfu
    dfu-util 0.9
    
    Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
    Copyright 2010-2016 Tormod Volden and Stefan Schmidt
    This program is Free Software and has ABSOLUTELY NO WARRANTY
    Please report bugs to http://sourceforge.net/p/dfu-util/tickets/
    
    Match vendor ID from file: 04d8
    Match product ID from file: ef99
    Opening DFU capable USB device...
    ID 04d8:ef99
    Run-time device DFU version 0100
    Claiming USB DFU Runtime Interface...
    Determining device status: state = dfuIDLE, status = 0
    dfu-util: WARNING: Runtime device already in DFU state ?!?
    Claiming USB DFU Interface...
    Setting Alternate Setting #0 ...
    Determining device status: state = dfuIDLE, status = 0
    dfuIDLE, continuing
    DFU mode device DFU version 0100
    Device returned transfer size 64
    Copying data from PC to DFU device
    Download        [=========================] 100%        16384 bytes
    Download done.
    state(2) = dfuIDLE, status(0) = No error condition is present
    Done!
```

## 4. Uso em comunicação USB serial para acesso terminal REPL

### 4.1. Expansion Board v2.x

A interface USB serial é vista no Linux como '/dev/ttyUSB0'.

Exemplo de ferramentas shell no Linux :
- 'screen' para acesso terminal REPL :
```
$ screen /dev/ttyACM0 115200
```
- [rshell](https://github.com/dhylands/rshell) :
```
$ rshell -p /dev/ttyUSB0
```

### 4.2. Expansion Board v3.x

A interface USB serial é vista no Linux como '/dev/ttyACM0'.

Exemplo de ferramentas shell no Linux :
- 'screen' para acesso terminal REPL :
```
$ screen /dev/ttyACM0 115200
```
- [rshell](https://github.com/dhylands/rshell) :
```
$ rshell -p /dev/ttyACM0
```


## 5. Configuração dos mini-jumpers

**Atenção : os mini-jumpers são fáceis de soltar com movimento.** Recomendo comprar mini-jumpers para ter de reserva caso 
perca parte dos mini-jumpers da Expansion Board.

Outra recomendação é somente usar os mini-jumpers em configurações necessárias caso queira economizar energia (modo deepsleep), 
vide [seção abaixo 'Consumo de energia'](#consumo-de-energia).

São 7 mini-jumpers que por default estão inseridos :
- TX : 'Enable TX signal', obrigatório estar inserido para se ter conexão USB serial (UART), conectado ao pino G0/P2;
- RTS : 'Enable RTS signal', obrigatório estar inserido para se ter controle de fluxo da conexão USB serial (UART), que é opcional, 
        conectado ao pino G6/P19;
- RX : 'Enable RX signal', obrigatório estar inserido para se ter conexão USB serial, conectado ao pino G1/P1;
- CTS : 'Enable CTS signal', obrigatório estar inserido para se ter controle de fluxo da conexão USB serial (UART), que é opcional, 
        conectado ao pino G7/P20;
- BAT : 'Enable VBATT signal', obrigatório estar inserido para se ter a tensão da bateria no pino VBATT/G3/P16 para ser 
        [lida por ADC do ESP32/xxPy](#10-leitura-da-tensão-da-bateria-via-divisor-de-tensão-e-pino-vbatt);
- LED : 'Enable LED signal', obrigatório estar inserido para se controlar o "User LED" via pino G16/P9, [vide exemplo abaixo](#uso-do-user-led);
- CHG : 'Enable CHG signal', inserido para se carregar bateria com 450 mA, senão carrega com 100 mA. Cuidado que o 
        [circuito carregador BQ24040](https://www.ti.com/product/BQ24040) de bateria Li-Ion/LiPo tem um 
        [limite de segurança de 10 horas de carga](https://forum.pycom.io/topic/821/lopy-battery/4?_=1592254492178), então 
        se a bateria tiver capacidade muito grande pode ser que não consiga carregar totalmente dependendo dessa configuração.


## 6. Uso do 'User button'

O botão de usuário ('User button') é do tipo mini-push button e tem o nome "BUTTON" na Expansion Board v2.x e 
"S1" na Expansion Board v3.x. Está conectado ao pino G17/P10, então a leitura é via pino digital de entrada.
Veja o ['Quick Usage Example' em 'Firmware & API Reference > Pycom Modules > machine > Pin'](https://docs.pycom.io/firmwareapi/pycom/machine/pin/) :
```
from machine import Pin

p_button = Pin('P10', mode=Pin.IN, pull=Pin.PULL_UP)
#p_button = Pin('G17', mode=Pin.IN, pull=Pin.PULL_UP)               # notação alternativa com 'Gn', usada pela Expansion Board desde 2015.
#p_button = Pin(Pin.exp_board.G17, mode=Pin.IN, pull=Pin.PULL_UP)   # forma alternativa de acessar a notação 'Gn' para pinos
#p_button = Pin(Pin.module.P10, mode=Pin.IN, pull=Pin.PULL_UP)      # forma alternativa de acessar a notação 'Pn' para pinos

p_button.value()   # 1 sem pressionar o botão 
    1
p_button.value()   # 0 pressionando o botão 
    0
```

## 7. Uso do 'User LED'

O "user LED" é controlado via pino G16/P9, desde que o mini-jumper 'LED' esteja inserido.
[O diagrama de pinagem da Expansion Board v2.x](https://docs.pycom.io/datasheets/boards/expansion2/) mostra que a lógica para 
ligar o LED é colocar a saída digital do pino G16/P9 em estado baixo (low).
Veja o ['Quick Usage Example' em 'Firmware & API Reference > Pycom Modules > machine > Pin'](https://docs.pycom.io/firmwareapi/pycom/machine/pin/) :
```
from machine import Pin

p_led = Pin('P9', mode=Pin.OUT)
#p_led = Pin('G16', mode=Pin.OUT)                # notação alternativa, citando nomenclatura 'Gn' usada pela Expansion Board desde 2015.
#p_led = Pin(Pin.exp_board.G16, mode=Pin.OUT)   # forma alternativa de acessar a notação 'Gn' para pinos
#p_led = Pin(Pin.module.P9, mode=Pin.OUT)       # forma alternativa de acessar a notação 'Pn' para pinos
p_led.value()   # 'user LED' é ligado ao inicializar o pino G16/P9 como saída digital, pois o valor default é 0 (low)
    0
p_led.toggle()  # chaveia o 'user LED', no caso de estado ligado para desligado
p_led.value()   # 'user LED' está desligado pois o pino G16/P9 está com valor 1 (high)
    1
p_led.value(0)  # 'user LED' ligado com o pino G16/P9 tendo valor 0 (low)
p_led.value(1)  # desliga o 'user LED' ao usar valor 1 (high)
p_led(0)        # outra forma alternativa do pino G16/P9 ter valor 0 (low), ligando o 'user LED'
p_led(1)        # desliga o 'user LED' ao usar valor 1 (high)
```

## 8. Uso do 'User button' para acionar 'User LED'

Modificação do exemplo de ['machine.pin.callback()'](https://docs.pycom.io/firmwareapi/pycom/machine/pin/), usando 
interrupção acionada quando o botão é apertado (pino G17/P10 muda de estado 1 para 0).
Cada vez que o botão é apertado o estado (aceso/apagado) do LED muda :
```
from machine import Pin

p_led = Pin('P9', mode=Pin.OUT)
p_led.value(1)
p_button = Pin('P10', mode=Pin.IN, pull=Pin.PULL_UP)
p_button.callback(Pin.IRQ_FALLING, lambda x: p_led.toggle())
```

## 9. Uso de microSD

Sobre uso da unidade microSD (uSD) da placa Expansion Board, os cartões uSD devem ter sistema de arquivos FAT16 ou FAT32, com 
capacidade de até 32 GB, vide [documentação da Pycom sobre o submódulo 'machine.SD'](https://docs.pycom.io/firmwareapi/pycom/machine/sd/).

A memória flash interna do xxPy é vista como partição '/flash', enquanto o cartão uSD como partição '/sd' após ser instanciado
o objeto 'sd' e montada a partição via 'os.mount()'. Os comandos usuais de (Micro)Python para lidar com arquivos são aplicáveis, 
como open/write/close/etc e os do [módulo 'os'](https://docs.pycom.io/firmwareapi/micropython/uos/). Mas as funções 'os.ilistdir()'
e 'os.statvfs()' só aparecem na [documentação de MicroPython oficial](https://docs.micropython.org/en/latest/library/uos.html#uos.statvfs).

Código interativo :
```
import machine
import os

sd = machine.SD()
sd.
    __class__       deinit          init            ioctl
    readblocks      writeblocks
os.
    __class__       __name__        remove          chdir
    dupterm         fsformat        getcwd          getfree
    ilistdir        listdir         mkdir           mkfat
    mount           rename          rmdir           stat
    statvfs         sync            umount          uname
    unlink          urandom
os.mount(sd, '/sd')     # monta a partição '/sd'
os.listdir('/sd')       # uSD com arquivo 'test.csv'
    ['test.csv']
os.listdir()            # por default mostra memória flash interna, '/flash'
    ['boot.py', 'cert', 'lib', 'main.py', 'sys']    
s = os.statvfs('/sd')   # 1o elemento é o tamanho do bloco em bytes, 3o é o número de blocos da partição, 4o é o número de blocos livres
s
    (4096, 4096, 1936437, 1936019, 1936019, 0, 0, 0, 0, 128)
s[0]*s[2]/1024          # tamanho da partição '/sd' em kB de um cartão uSD de 8 GB
    7745748.0
s[0]*s[3]/1024          # espaço livre na partição '/sd' em kB
    7744076.0   
os.getfree('/sd')       # comando mais simples para mostrar o espaço livre na partição '/sd' em kB
    7744076
os.getcwd()             # o diretório default continua sendo na memória flash interna, '/flash', mesmo montando um uSD
    '/flash'
os.chdir('/sd')         # mudando o diretório default para a partição '/sd'
os.getcwd()
    '/sd'
f = open('/sd/test.csv', 'a')   # arquivo aberto para gravação em modo 'append' (adiciona ao arquivo se já existente, senão cria arquivo)
f.write('uSD with {} kB free\n'.format(os.getfree('/sd')))
f.close()               # fecha arquivo
os.sync()               # sincroniza buffers de gravação dos sistemas de arquivos
os.umount('/sd')        # desmonta a partição '/sd'
```

Código robusto para uso em scripts MicroPython via uso de 'try/except' para gerenciamento de erros :
```
import machine
import os

try:
    sd = machine.SD()
    os.mount(sd, '/sd')
    flag_uSD = True
except OSError as e:
    print('Error {} mounting uSD card'.format(e))
    flag_uSD = False

if flag_uSD:
    try:
        datalog_filename = '/sd/log.csv'
        with open(datalog_filename, 'a') as logfile:
            logfile.write('Pycom MicroPython v{}\n'.format(os.uname().release))
        os.sync()
    except OSError as e:
        print("Error {} writing to file '{}'".format(e, datalog_filename))

if flag_uSD:
    try:
        os.umount('/sd')
    except OSError as e:
        print('Error {} umounting uSD card'.format(e))
```


## 10. Leitura da tensão da bateria via divisor de tensão e pino VBATT

A placa Expansion Board tem divisor de tensão, com 2 resistores, conectado ao pino VBATT/G3/P16 para ler a tensão da bateria 
Li-Ion/Li-Po, que pode chegar a 4,2 V, sem estourar o limite de 3,3V do ADC do ESP32/xxPy.
Os valores dos resistores são diferentes para as versões da Expansion Board :
- v2.x, 56 kOhm (entre GND e pino G3/P16) e 115 kOhm (entre bateria e pino G3/P16), ou seja, a tensão da bateria é 
reduzida para (56/171) = 32,75%, p. e., 4,2 V se torna 1,375 V;
- v3.x, 1 MOhm (entre GND e pino G3/P16) e 1 MOhm (entre bateria e pino G3/P16), ou seja, a tensão da bateria é 
reduzida para (1/2) = 50%, p. e., 4,2 V se torna 2,1 V.

Lembrar que o ADC do ESP32 tem diversos problemas : offset (60-130 mV), não-linearidade e alto ruído (logo a resolução efetiva é uns
4-5 bits menor que a nominal), vide [comparação com ADS1115](https://github.com/bboser/IoT49/blob/master/doc/analog_io.md).

Código interativo optimizado para Expansion Board v2.x, usando escala de 6 dB (0,0-2,195 V) do ADC do ESP32/xxPy para aproveitar melhor 
a resolução de 12 bits, com o divisor de tensão (115 kOhm + 56 kOhm) a escala de leitura da tensão da bateria é 0,0-6,702 V :
```
import machine

adc = machine.ADC()
adc.
    __class__       ATTN_0DB        ATTN_11DB       ATTN_2_5DB
    ATTN_6DB        channel         deinit          init
    vref            vref_to_pin
adcP16 = adc.channel(pin='P16', attn=machine.ADC.ATTN_6DB)
adcP16.
    __class__       value           deinit          init
    value_to_voltage                voltage
adcP16.voltage()   # 1,390 V no pino G3/P16, ao ter alimentação via cabo USB
    1390
(171*_)//56        # 4,244 V no conector da bateria, ao ter alimentação via cabo USB
    4244
adcP16.voltage()   # 1,179 V no pino G3/P16, ao ter alimentação via bateria de 3,7 V nominais
    1179
(171*_)//56        # 3,600 V no conector da bateria, ao ter alimentação via bateria de 3,7 V nominais
    3600
```

Código interativo optimizado para Expansion Board v3.x, usando escala de 11 dB (0,0-3,903 V, porém limitada a 3,3 V) do ADC do 
ESP32/xxPy, com o divisor de tensão (1 MOhm + 1 MOhm) a escala de leitura da tensão da bateria é 0,0-6,6 V :
```
import machine

adc = machine.ADC()
adc.
    __class__       ATTN_0DB        ATTN_11DB       ATTN_2_5DB
    ATTN_6DB        channel         deinit          init
    vref            vref_to_pin
adcP16 = adc.channel(pin='P16', attn=machine.ADC.ATTN_11DB)
adcP16.
    __class__       value           deinit          init
    value_to_voltage                voltage
adcP16.voltage()   # 2,413 V no pino G3/P16, ao ter alimentação via cabo USB
    2413
(2*_)//1            # 4,826 V no conector da bateria (???), ao ter alimentação via cabo USB
    4826
adcP16.voltage()    # 1,810 V no pino G3/P16, ao ter alimentação via bateria de 3,7 V nominais
    1810
(2*_)//1            # 3,620 V no conector da bateria (???), ao ter alimentação via bateria de 3,7 V nominais
    3620
```

Código para uso em scripts MicroPython, com cálculo de média e desvio padrão das medidas do ADC via 
[módulo MicroPython 'statistics'](https://github.com/rcolistete/MicroPython_Statistics). Tire/coloque 
comentários para escolher entre Expansion Board v2.x e v3.x :
```
import machine
import statistics
    
NUM_ADC_READINGS = const(50)

adc = machine.ADC()
adc.vref(1126)  # put here ESP32 VRef measured in mV with a voltmeter
# adcP16 = adc.channel(pin='P16', attn=machine.ADC.ATTN_6DB) # For Expansion Board v2.x
adcP16 = adc.channel(pin='P16', attn=machine.ADC.ATTN_11DB)  # For Expansion Board v3.x
samples_voltage = [0]*NUM_ADC_READINGS
for i in range(NUM_ADC_READINGS):
#    samples_voltage[i] = (171*adcP16.voltage())//56   # Expansion Board v2.x has voltage divider (115K + 56K) -> voltage scale = [0, 6702] mV
    samples_voltage[i] = (2*adcP16.voltage())//1       # Expansion Board v3.x has voltage divider (1M + 1M) -> voltage scale = [0, 6600] mV
batt_mV = round(statistics.mean(samples_voltage))
batt_mV_error = round(statistics.stdev(samples_voltage))
```


## 11. Consumo de energia

### 11.1. Alimentação via cabo USB com medidor USB [UM25C](https://www.aliexpress.com/item/32855845265.html)

Tensão Vusb = 5,06-5,08 V.

WiPy 3 (com PyBytes off, WiFi off, LED RBG com heartbeat off) + Expansion Board v3.0 (sem uSD, com todos mini-jumpers inseridos) :
* i = 45,0-45,2 com 'User LED' (vermelho) desligado;
* i = 48,3-48,6 mA com **'User LED' (vermelho) ligado, gasta 3,1-3,6 mA a mais**;

#### 11.1.1. Expansion Board v2.x/v3.x com WiPy 3 em modo ativo, alimentação USB

WiPy 3 com PyBytes off, WiFi off, LED RBG com heartbeat off.

Para comparação, **WiPy 3 sem Expansion Board, com bateria (Vbat = 3,61-3,66 V) alimentando diretamente nos pinos VIN e GND,**
**consome 34,5-34,7 mA em modo ativo, i. e., Pbat = 121-123 mW**.

Foram testadas configurações sem e com uSD 32GB Samsung Evo UHS-I.

| Expansion Board | i (ma) com todos mini-jumpers | i (ma) só com mini-jumpers TX/RX | i (ma) sem mini-jumpers |
|:----------------|:-----------------------------:|:--------------------------------:|:-----------------------:|
| v2.1A           |           38,8-39,6           |            38,3-39,0             |       38,2-39,0         |
| v3.0            |           43,5-44,2           |            42,8-43,7             |       42,9-43,7         |
| v3.1            |           43,3-44,0           |            42,8-43,5             |       42,7-43,4         |

Notar que em modo ativo com alimentação USB:
- **Expansion Board v2.1A consome Pusb = 193-201 mW (61% a mais do que não ser usada), v3.x consome Pusb = 216-224 W (80% a mais)**;
- **a Expansion Board v2.1A consome em torno de 11% menos que as Expansion Board v3.x**;
- com todos mini-jumpers (i. e., com LED mini-jumper), o consumo aumenta 0,5-0,6 mA em relação a não ter mini-jumpers;
- o consumo é aproximadamente o mesmo sem e com mini-jumpers TX/RX;

#### 11.1.2. Expansion Board v2.x/v3.x com WiPy 3 em [modo light sleep](https://docs.pycom.io/firmwareapi/pycom/machine/), alimentação USB

WiPy 3 com PyBytes off, WiFi off, LED RBG com heartbeat off, sem uSD.

Para comparação, **WiPy 3 sem Expansion Board, com bateria (Vbat = 4,15-4,16 V) alimentando diretamente nos pinos VIN e GND,** 
**consome 1,94-2,02 mA em modo light sleep, i. e., Pbat = 7,80-8,18 mW**.

| Expansion Board | i (ma) só com mini-jumpers TX/RX |
|:----------------|:--------------------------------:|
| v2.1A           |            13,9-14,3             |
| v3.0            |            17,1-17,4             |
| v3.1            |            17,1-17,5             |

Notar que em modo light sleep com alimentação USB:
- **Expansion Board v2.1A consome Pusb = 70,3-72,6 mW (8,9x do que não ser usada), v3.x consome Pusb = 86,5-88,9 mW (11x)**;
- **a Expansion Board v2.1A consome em torno de 18% menos que as Expansion Board v3.x**;
- sobre mini-jumpers e uSD, veja as análises acima e abaixo.


#### 11.1.3. Expansion Board v2.x/v3.x com WiPy 3 em [modo deep sleep](https://docs.pycom.io/firmwareapi/pycom/machine/), alimentação USB

Para comparação, **WiPy 3 sem Expansion Board, com bateria (Vbat = 3,61-3,66 V) alimentando diretamente nos pinos VIN e GND,**
**consome 20-21 uA em modo deep sleep, i. e., Pbat = 0,0722-0,0769 mW**.

| Expansion Board |  xxPy  |           uSD           | i (ma) com todos mini-jumpers | i (ma) só com mini-jumpers TX/RX |
|:----------------|:------:|:-----------------------:|:-----------------------------:|:--------------------------------:|
| v2.1A           |   -    |            -            |         12,5-12,8             |            12,5-12,8             |
| v2.1A           | WiPy 3 |            -            |         12,7-13,0             |            12,5-12,9             |
| v2.1A           | WiPy 3 |  32GB Samsung Evo UHS-I |         13,1-13,4             |            12,9-13,3             |
| v3.0            |   -    |            -            |         15,3-15,6             |            15,3-15,6             |
| v3.0            | WiPy 3 |            -            |         15,7-16,0             |            15,3-15,7             |
| v3.0            | WiPy 3 |  32GB Samsung Evo UHS-I |         16,1-16,5             |            15,8-16,2             |
| v3.1            |   -    |            -            |         15,2-15,6             |            15,2-15,6             |
| v3.1            | WiPy 3 |            -            |         15,7-16,0             |            15,5-15,8             |
| v3.1            | WiPy 3 |  32GB Samsung Evo UHS-I |         16,1-16,5             |            15,9-16,2             |

Notar que em modo deep sleep com alimentação USB :
- sem xxPy, as placas Expansion Board v2.1A/v3.x sozinhas consomem independentemente da configuração de mini-jumpers e cartão 
uSD inserido ou não;
- **a Expansion Board v2.1A consome em torno de 19% menos que as Expansion Board v3.x**;
- o consumo é o mesmo sem e com mini-jumpers TX/RX;
- com todos mini-jumpers (i. e., com LED mini-jumper), o consumo aumenta 0,1-0,3 mA em relação a não ter mini-jumpers;
- o cartão uSD, mesmo sem estar sendo usado, nem montado, consume energia, basta estar inserido. 
**A mera inserção de cartão uSD 32GB Samsung Evo UHS-I gasta a mais 0,4-0,5 mA**, tal consumo adicional depende da modelo do cartão uSD (marca, 
capacidade, etc), só medindo para saber.

Então **para ter consumo um pouco mais baixo em modo deep sleep com alimentação USB, use Expansion Board** :
- **v2.1**;
- **sem LED mini-jumper**;
- **sem uSD**, mas se precisar teste diferentes modelos e meça o consumo de energia para escolher qual gasta menos, pois pode 
variar 10x.

**Para se ter baixo consumo (< 1 mA) em modo deep sleep com Expansion Board deve-se evitar alimentação USB e adotar alimentação via bateria 
Li-Ion/Li-Po, vide seção 11.2.3 abaixo.**


### 11.2. Alimentação via conector da bateria, medindo com multímetro

Com 3,3 V no conector da bateria o xxPy (WiPy 3, LoPy4, etc) não liga se estiver na Expansion Board.

#### 11.2.1. Expansion Board v2.x/v3.x com WiPy 3 em modo ativo, alimentação por bateria

WiPy 3 com PyBytes off, WiFi off, LED RBG com heartbeat off. Tensão da bateria, Vbat = 3,61-3,66 V.

Para comparação, **WiPy 3 sem Expansion Board, com bateria (Vbat = 3,61-3,66 V) alimentando diretamente nos pinos VIN e GND,**
**consome 34,5-34,7 mA em modo ativo, i. e., Pbat = 121-123 mW**.

Foram testadas configurações sem e com uSD 8GB Sandisk UHS-I, 16GB Toshiba M203 UHS-I e 32GB Samsung Evo UHS-I.

| Expansion Board | i (ma) com todos mini-jumpers | i (ma) só com mini-jumpers TX/RX | i (ma) sem mini-jumpers |
|:----------------|:-----------------------------:|:--------------------------------:|:-----------------------:|
| v2.1A           |           35,0-35,7           |            34,6-35,2             |       34,6-35,2         |
| v3.0            |             39-42             |              39-42               |       34,9-35,3         |
| v3.1            |           69,6-70,2           |            68,8-69,4             |       34,5-35,0         |

Notar que em modo ativo com alimentação por bateria :
- **sem mini-jumpers, as 3 placas Expansion Board consomem o mesmo, Pusb = 125-131 mW (5% a mais do que não ser usada)**;
- a placa **Expansion Board v2.1A consome o mesmo sem e com mini-jumpers TX/RX**, enquanto que 
**Expansion Board v3.0 tem acréscimo de 4-7 mA (14-26 mW)** e **Expansion Board v3.1 consome a mais uns 33,8-34,9 mA (122-128 mW)**.

Então **para ter baixo consumo em modo ativo, use Expansion Board** :
- **sem mini-jumpers (i. e., sem LED e TX/RX)**, se possível;
- **v2.1** se precisar usar mini-jumpers TX/RX (modo USB serial).

#### 11.2.2. Expansion Board v2.x/v3.x com WiPy 3 em [modo light sleep](https://docs.pycom.io/firmwareapi/pycom/machine/), alimentação por bateria

WiPy 3 com PyBytes off, WiFi off, LED RBG com heartbeat off, sem uSD.

Para comparação, **WiPy 3 sem Expansion Board, com bateria (Vbat = 4,15-4,16 V) alimentando diretamente nos pinos VIN e GND,** 
**consome 1,94-2,02 mA em modo light sleep, i. e., Pbat = 7,80-8,18 mW**.

Como a tensão da bateria variou entre 3,82-4,15 V, foi calculada a potência P = V*i.

| Expansion Board | P (mW) com todos mini-jumpers | P (mW) só com mini-jumpers TX/RX | P (mW) sem mini-jumpers |
|:----------------|:-----------------------------:|:--------------------------------:|:-----------------------:|
| v2.1A           |            8,1-8,4            |             7,9-8,2              |        7,9-8,2          |
| v3.0            |           21,1-28,2           |            21,1-28,2             |        8,0-8,2          |
| v3.1            |              120              |               120                |        7,2-7,3          |

Notar que em modo light sleep com alimentação por bateria :
- Expansion Board v2.1/v3.x sem mini-jumpers, o consumo é entre 7-8 mW (1,74-1,99 mA @ 4,14 V);
- com mini-jumpers TX/RX, a potência consumida com Expansion Board v3.0 aumenta para 21-28 mW (5,1-6,8 mA @ 4,13-4,14 V) 
e com Expansion Board v3.1 para 120 mW (29,9-30,0 mA @ 4,01 V);
- sobre uso uSD, veja as análises abaixo.

Então **para ter baixo consumo em modo light sleep, use Expansion Board** :
- **sem mini-jumpers (i. e., sem LED e TX/RX)**, se possível;
- **v2.1** se precisar usar mini-jumpers TX/RX (modo USB serial).

#### 11.2.3. Expansion Board v2.x/v3.x com WiPy 3 em [modo deep sleep](https://docs.pycom.io/firmwareapi/pycom/machine/), alimentação por bateria

Tensão da bateria, Vbat = 3,61-3,66 V.

Para comparação, **WiPy 3 sem Expansion Board, com bateria (Vbat = 3,61-3,66 V) alimentando diretamente nos pinos VIN e GND,**
**consome 20-21 uA em modo deep sleep, i. e., Pbat = 0,0722-0,0769 mW**.

| Expansion Board |  xxPy  |           uSD           | i (ua) com todos mini-jumpers | i (ua) só com mini-jumpers TX/RX |
|:----------------|:------:|:-----------------------:|:-----------------------------:|:--------------------------------:|
| v2.1A           |   -    |            -            |            136                |               136                |
| v2.1A           | WiPy 3 |            -            |            226                |               156                |
| v2.1A           | WiPy 3 |    8GB Sandisk UHS-I    |            306                |               236                |
| v2.1A           | WiPy 3 | 16GB Toshiba M203 UHS-I |            283                |               213                |
| v2.1A           | WiPy 3 |  32GB Samsung Evo UHS-I |            817                |               747                |
| v3.0            |   -    |            -            |              3                |                 3                |
| v3.0            | WiPy 3 |            -            |            302                |               233                |
| v3.0            | WiPy 3 |    8GB Sandisk UHS-I    |            383                |               313                |
| v3.0            | WiPy 3 | 16GB Toshiba M203 UHS-I |            359                |               290                |
| v3.0            | WiPy 3 |  32GB Samsung Evo UHS-I |            897                |               825                |
| v3.1            |   -    |            -            |             12                |                12                |
| v3.1            | WiPy 3 |            -            |            102                |                34                |
| v3.1            | WiPy 3 |    8GB Sandisk UHS-I    |            183                |               114                |
| v3.1            | WiPy 3 | 16GB Toshiba M203 UHS-I |            160                |                90                |
| v3.1            | WiPy 3 |  32GB Samsung Evo UHS-I |            692                |               623                |

Notar que em modo deep sleep com alimentação por bateria :
- sem xxPy, a placa **Expansion Board v2.1A sozinha consome 136 uA, v3.0 consome 3 uA, v3.1 consome 12 uA**, independente 
da configuração de mini-jumpers e cartão uSD inserido ou não;
- com WiPy3 (em modo deep sleep), sem uSD, a placa **Expansion Board v2.1A consome 156 uA (226 uA c/ todos mini-jumpers)**, 
**v3.0 consome 233 uA (302 uA), v3.1 consome 34 uA (102 uA)**;
- **retirando todos mini-jumpers exceto TX/RX economiza de 68-72 uA**. O mini-jumper LED que consome mais e TX/RX em modo 
deep sleep não faz diferença;
- o cartão uSD, mesmo sem estar sendo usado, nem montado, consume energia, basta estar inserido. 
**A mera inserção de cartão uSD gasta a mais 57-595 uA**, tal consumo adicional depende da modelo do cartão uSD (marca, 
capacidade, etc), só medindo para saber.

Então **para ter baixíssimo consumo em modo deep sleep, use Expansion Board** :
- **v3.1**;
- **sem LED mini-jumper**;
- **sem uSD**, mas se precisar teste diferentes modelos e meça o consumo de energia para escolher qual gasta menos, pois pode 
variar 10x.

Vide mais comparações no tópico 
[MicroSD power consumption on Expansion Board, PyTrack, PySense, etc](https://forum.pycom.io/topic/3136/microsd-power-consumption-on-expansion-board-pytrack-pysense-etc) 
do fórum Pycom.

(A FAZER) :
- em várias configurações fazer medidas com fonte de tensão regulável mais estável (p. e., 3,7 V) como se fosse bateria conectada 
nos pinos JST PH 2.0;
- usar fonte de tensão regulável para variar a tensão nos pinos JST PH 2.0 e medir a potência dissipada em certa configuração.


## 12. Voltagem nos pinos Vin e Vbatt/G3/P16

As medidas abaixo são para tentar esclarecer a diferença de voltagem de alimentação (por bateria ou USB), Vin e 
Vbatt/G3/P16.

O diagrama de blocos do [manual oficial da Pycom, da Expansion Board v2.0](https://github.com/wipy/wipy/blob/master/docs/User_manual_exp_board.pdf) :
- mostra que Expansion Board tem saída para pino Vin da placa xxPy, vindo de alimentação USB ou de bateria e passando 
por diferentes circuitos integrados (que obviamente geram queda de voltagem);
- não mostra exatamente onde tem saída para pino Vbatt/G3/P16, por exemplo, se antes ou depois do "Reverse Polarity 
Protection", logo não há confirmação se é 100% da tensão da bateria que chega no divisor de tensão conectado ao pino 
Vbatt/G3/P16 da placa xxPy.

Veja na [seção 10](#10-leitura-da-tensão-da-bateria-via-divisor-de-tensão-e-pino-vbatt) que o pino Vbatt/G3/P16 (V) tem 
a tensão (de ponto próximo) da bateria reduzida para :
- (56/171) = 32,75% na Expansion Board v2.x;
- (1/2) = 50% na Expansion Board v3.x.
Mas considere uma tolerância de 1% dos resistores do divisor de tensão.

| Expansion Board |  xxPy  |        Alimentação         |    Vin (V)   | Vbatt/G3/P16 (V) |
|:----------------|:------:|:--------------------------:|:------------:|:----------------:|
| v2.1A           |   -    | Vbat = 4,130-4,131 V       | 4,130-4,131  |      1,348       |
| v2.1A           | WiPy 3 | Vbat = 4,129 V             |    4,129     |      1,347       |
| v2.1A           |   -    | Vusb = 4,819 V sem bateria |    4,804     |      1,305       |
| v2.1A           | WiPy 3 | Vusb = 5,125 V sem bateria |    5,108     |      1,305       |
| v2.1A           |   -    | Vusb = 5,121 V com bateria |    5,058     |      1,368       |
| v2.1A           | WiPy 3 | Vusb = 5,122 V com bateria |    5,060     |      1,368       |
| v3.0            |   -    | Vbat = 4,130 V             |    4,130     |      1,976       |
| v3.0            | WiPy 3 | Vbat = 4,130-4,132 V       | 4,129-4,130  |   1,972-1,975    |
| v3.0            |   -    | Vusb = 5,125 V sem bateria | 5,005-5,006  |      2,395       |
| v3.0            | WiPy 3 | Vusb = 5,125 V sem bateria |    4,927     |   2,353-2,357    |
| v3.0            |   -    | Vusb = 5,113 V com bateria |    4,863     |      2,328       |
| v3.0            | WiPy 3 | Vusb = 5,116 V com bateria |    4,820     |      2,303       |
| v3.1            |   -    | Vbat = 4,130 V             |    4,130     |      1,966       |
| v3.1            | WiPy 3 | Vbat = 4,121 V             |    4,118     |   1,958-1,959    |
| v3.1            |   -    | Vusb = 5,125 V sem bateria | 4,869-4,870  |      2,317       |
| v3.1            | WiPy 3 | Vusb = 5,125 V sem bateria | 4,858-4,859  |   2,302-2,307    |
| v3.1            |   -    | Vusb = 5,110 V com bateria |    4,625     |      2,165       |
| v3.1            | WiPy 3 | Vusb = 5,099 V com bateria |    4,579     |   2,153-2,154    |

Notar que :
- voltagem no pino Vin <> voltagem no pino Vbatt/G3/P16 após dividir pelo fator do divisor de tensão, a 
diferença pode ser entre menos de 1% a uns 7%;
- quando alimentado por bateria, Vin é igual ou muito pouco (mV) menor que a tensão da bateria, Vbat;
- quando tem alimentação USB, Vin é menor (centésimos a décimos de V) que a tensão USB, a diferença aumenta 
com a corrente elétrica consumida, por exemplo quando está sendo carregada uma bateria;
- a voltagem no pino Vbatt/G3/P16, após dividir pelo fator do divisor de tensão, é bem próxima da voltagem 
da bateria no caso da Expansion Board v2.1 (erro < 1%), mas com Expansion Board v3.x o valor é uns 5% menor;
- via leitura de pino Vbatt/G3/P16 é difícil de detectar entre alimentação USB versus alimentação via bateria 
muito carregada, então para detectar presença de alimentação USB é melhor ler Vin (via outro divisor de tensão 
e ADC em outro pino).