# Usando WiPy 3

##### Autor : Roberto Colistete Jr. (roberto.colistete arroba gmail.com)
##### Última atualização : 08/07/2020.

WiPy 3 é uma placa lançada pela Pycom no final de 2017 com microcontrolador ESP32, memória PSRAM de 4 MB e memória 
flash de 8 MB, de alta qualidade de construção, [certificação (CE e FCC)](https://docs.pycom.io/documents/certificates/), 
com baixíssimo consumo em modo deepsleep (19-20 uA). [WiPy 3.0 na Pycom Store custa EUR 19.95](https://pycom.io/product/wipy-3-0/).

<img src="https://pycom.io/wp-content/uploads/2018/08/wipySide-1.png" alt="WiPy 3 com pinos" width="400"/>



## 1. Documentação do WiPy 3 e MicroPython

- [Pycom MicroPython Documentation](https://docs.pycom.io/);
- [WiPy 3 datasheet, pinout and many links](https://docs.pycom.io/datasheets/development/wipy3/);
- [Getting Started (with WiPy 3)](https://docs.pycom.io/gettingstarted/connection/wipy/).
<img src="https://docs.pycom.io/gitbook/assets/wipy3-pinout.png" alt="Descrição da pinagem do WiPy3" width="1000"/>

Especificações do WiPy 3, vide também [site da Expressif sobre ESP32](https://www.espressif.com/en/products/socs/esp32/overview) :
- ESP32 (ESP32D0WDQ6 rev. 1) com 2x 32 bit Xtensa 32-bit LX6 @ 160/240 MHz, suporte a número de ponto flutuante com precisão simples;
- 520 kB SRAM e 4 MB PSRAM (via QSPI);
- 448 kB ROM e 8 MB de memória flash (via QSPI);
- WiFi 802.11b/g/n 150 MBps, Bluetooth v4.2 BR/EDR e BLE;
- 34 GPIO (24 pinos expostos no WiPy 3, mas pino P2 é para LED RGB/boot, pino P12 é para antena WiFi/modos de boot);
- 18 canais de ADC de 12 bits, 2 canais de DAC de 8 bits, 16 portas PWM;
- 2x I2C (400 kHz master/slave), 4x SPI (up to 80 MHz master/slave), 2x I2S;
- 4x timers (64 bits com prescalers de 16-bits), 13 canais DMA, 3x UARTs;
- RTC com cristal de 32,768 kHz e 16 kB SRAM (8 kB slow SRAM, 8 kB fast SRAM);
- antena interna para WiFi, conector para antena externa para WiFi;
- RGB LED WS2812;
- botão de reset;
- [regulador de tensão LM3281 para 3,3V](https://www.ti.com/product/LM3281);
- dimensões sem pinos : 42 mm x 20 mm x 2.5 mm;
- peso sem pinos : 5 g.

WiPy 3 tem a mais em relação a WiPy 2 (lançado em 09/2016, com 1as entregas em 10/2016) :
- [ESP32D0WDQ6 revision 1 (x revision 0)](https://www.espressif.com/sites/default/files/documentation/eco_and_workarounds_for_bugs_in_esp32_en.pdf);
- 4 MB de RAM (x 520 kB);
- 8 MB de memória flash (x 4 MB).


Nos códigos-fonte de testes :
* digite tecla 'Tab' após '.' em final de linha para ver a lista de atributos do objeto;
* saídas/resultados estão indentados com 1 tabulação a mais.



## 2. Testes e exemplos com MicroPython

- usando firmware Pycom MicroPython oficial 1.20.2.rc7 (de 04/05/2020, mas foi anunciado oficialmente só em 
10/06/2020 em [Pycom MicroPython releases](https://github.com/pycom/pycom-micropython-sigfox/releases)) :
```
$ screen /dev/ttyACM0 115200
Pycom MicroPython 1.20.2.rc7 [v1.11-6d01270] on 2020-05-04; WiPy with ESP32
import gc
gc.collect()
gc.mem_free()
    2560704   # 2.500,687 kB de RAM livre
```

- [módulo 'os'](https://docs.pycom.io/firmwareapi/micropython/uos/) para ver versão de MicroPython, tamanho do sistema 
de arquivos da memória flash, etc :
```
import os
os.
    __class__       __name__        remove          chdir
    dupterm         fsformat        getcwd          getfree
    ilistdir        listdir         mkdir           mkfat
    mount           rename          rmdir           stat
    statvfs         sync            umount          uname
    unlink          urandom
os.uname()
    (sysname='WiPy', nodename='WiPy', release='1.20.2.rc7', version='v1.11-6d01270 on 2020-05-04', machine='WiPy with ESP32', pybytes='1.4.0')
os.statvfs('/flash')
    (4096, 4096, 1024, 1016, 1016, 0, 0, 0, 0, 1022)   # 4MB as file system from total of 8 MB of flash memory
```

- [módulo 'machine'](https://docs.pycom.io/firmwareapi/pycom/machine/), frequência do ESP32 (160 MHz, não dá para alterar), etc :
```
import machine
machine.
    __class__       __name__        main            ADC
    BROWN_OUT_RESET                 CAN             DAC
    DEEPSLEEP_RESET                 HARD_RESET      I2C
    PIN_WAKE        PWM             PWRON_RESET     PWRON_WAKE
    Pin             RMT             RTC             RTC_WAKE
    SD              SOFT_RESET      SPI             Timer
    Touch           UART            ULP_WAKE        WAKEUP_ALL_LOW
    WAKEUP_ANY_HIGH                 WDT             WDT_RESET
    deepsleep       disable_irq     enable_irq      flash_encrypt
    freq            idle            info            mem16
    mem32           mem8            pin_sleep_wakeup
    remaining_sleep_time            reset           reset_cause
    rng             secure_boot     sleep           temperature
    unique_id       wake_reason
machine.freq()
    160000000
machine.info()
    ---------------------------------------------
    System memory info (in bytes)
    ---------------------------------------------
    MPTask stack water mark: 8624
    ServersTask stack water mark: 3684
    TimerTask stack water mark: 2212
    IdleTask stack water mark: 608
    System free heap: 407204
    ---------------------------------------------
machine.temperature()   # in F
    117
(_ - 32)/1.8            # in C
    47.22223
machine.unique_id()
    b'0\xae\xa4x\xe0\xb8'
import ubinascii
ubinascii.hexlify(machine.unique_id())   # = Device ID = WiFi MAC address
    b'30aea478e0b8'
```

- [módulo 'micropython'](https://docs.pycom.io/firmwareapi/micropython/micropython/) :
```
import micropython
micropython.
    __class__       __name__        const
    alloc_emergency_exception_buf   heap_lock       heap_unlock
    kbd_intr        mem_info        opt_level       qstr_info
    stack_use
micropython.mem_info()
    stack: 736 out of 11264
    GC: total: 2561344, used: 1872, free: 2559472
    No. of 1-blocks: 48, 2-blocks: 15, max blk sz: 8, max free sz: 159779   
```

- help :
```
help()
    Welcome to MicroPython!
    For online docs please visit http://docs.pycom.io

    Control commands:
    CTRL-A        -- on a blank line, enter raw REPL mode
    CTRL-B        -- on a blank line, enter normal REPL mode
    CTRL-C        -- interrupt a running program
    CTRL-E        -- on a blank line, enter paste mode
    CTRL-F        -- on a blank line, do a hard reset of the board and enter safe boot

    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (pybytes, etc) da Pycom :
```
help('modules')
    _OTA              _pybytes_library  math              uerrno
    __main__          _pybytes_main     micropython       uerrno
    _boot             _pybytes_protocol network           uhashlib
    _coap             _pybytes_pymesh_config              os                uio
    _flash_control_OTA                  _terminal         pycom             ujson
    _main             _thread           queue             umachine
    _main_pybytes     _urequest         re                uos
    _mqtt             array             select            uqueue
    _mqtt_core        binascii          socket            ure
    _msg_handl        builtins          ssl               uselect
    _periodical_pin   cmath             struct            uselect
    _pybytes          crypto            sys               usocket
    _pybytes_ca       errno             time              ussl
    _pybytes_config   framebuf          ubinascii         ustruct
    _pybytes_config_reader              gc                ubinascii         utime
    _pybytes_connection                 hashlib           ucollections      utimeq
    _pybytes_constants                  json              ucrypto           uzlib
    _pybytes_debug    machine           uctypes
    Plus any modules on the filesystem
```

- [LED RGB](https://docs.pycom.io/firmwareapi/pycom/pycom/), vide [tutorial](https://docs.pycom.io/tutorials/all/rgbled/). 
"By default the heartbeat LED flashes in blue colour once every 4s to signal that the system is alive".  
Para desabilitar heartbeat RGB LED, assim i oscila menos (somente na faixa de 0,2 mA), há 2 formas :
```
import pycom
pycom.heartbeat_on_boot(False)   # Desabilita a pulsação (heartbeat) do LED RGB, efetivo só no próximo boot
pycom.heartbeat(False)           # Desabilita a pulsação (heartbeat) do LED RGB, imediatamente
pycom.heartbeat()                # Mostra o estado da configuração de pulsação (heartbeat) do LED RGB
    False
pycom.rgbled(0x0000ff)           # Azul, brilho máximo
pycom.rgbled(0x000010)           # Azul, brilho fraco
pycom.rgbled(0x00ff00)           # Verde, brilho máximo
pycom.rgbled(0xff0000)           # Vermelho, brilho máximo
pycom.rgbled(0xffffff)           # Branco, brilho máximo
pycom.rgbled(0x000000)           # Desligado
pycom.heartbeat(True)            # Habilita a pulsação (heartbeat) do LED RGB
```

- [desabilitar PyBytes](https://forum.pycom.io/topic/5649/new-firmware-release-1-20-2-rc3) :
```
import pycom
pycom.pybytes_on_boot(False)         # efetivo só no próximo boot, normalmente já vem com False após gravar firmware sem habilitar PyBytes
```

- [desabilitar WiFi](https://development.pycom.io/firmwareapi/pycom/pycom/). O default após o firmware ser gravado é ter WiFi 
habilitado em modo AP (Access Point) :
```
import network
wlan = network.WLAN(mode=network.WLAN.STA)
wlan.mode()   # 1 = wlan.STA
wlan.deinit()
#
import network
wlan = network.WLAN(mode=network.WLAN.AP, ssid='WiPy3')
wlan.mode()   # 2 = wlan.AP
# ou  :
import pycom
pycom.wifi_on_boot(False)            # efetivo só no próximo boot, normalmente já vem com True após gravar firmware
```

- [desabilitar BLE - Bluetooth Low Energy](https://docs.pycom.io/firmwareapi/pycom/network/bluetooth/), mas i varia fração de mA, 
difícil de medir (inclusive parece que aumenta ao rodar o 'ble.deinit()':
```
import network
ble = network.Bluetooth()
ble.deinit()
```

- [light sleep](https://development.pycom.io/firmwareapi/pycom/machine/), modo de economia de energia leve em que [a CPU é 
pausada e periféricos desligados (inclusive WiFi e Bluetooth)](https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/#esp32-light-sleep), 
continuando de onde parou após terminar :
```
import machine
machine.sleep(60000)   # 60000 ms = 60,000 s
```

- [deep sleep](https://development.pycom.io/firmwareapi/pycom/machine/), modo de economia de energia que [desliga tudo menos 
o RTC](https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/#esp32-deep-sleep), dando reboot após terminar :
```
import machine
machine.deepsleep(60000)   # 60000 ms = 60,000 s
```


A FAZER exemplos sobre :
- GPIO, entrada e saída;
- I2C;
- SPI;
- ADC;
- DAC;
- WiFi;
- RTC e RAM;
- NVRAM (nvs_*);
- arquivos em '/flash'
- etc.



## 3. Consumo de energia

Firmware : Pycom MicroPython 1.20.2.rc7 [v1.11-6d01270] on 2020-05-04; WiPy with ESP32


### 3.1. Modos deep sleep x light sleep x ativo

Alimentação Vusb usa cabo USB e medidor USB [UM25C](https://www.aliexpress.com/item/32855845265.html). 
Alimentação Vbat é via conector da bateria, medindo com multímetro.

WiPy 3 (com PyBytes off, WiFi off, LED RBG com heartbeat off) + Expansion Board v3.1 sem uSD, sem mini-jumpers.

| Expansion Board |       Modo      |     Alimentação     |    i (ma)   |    P (mW)     |
|:---------------:|:---------------:|:-------------------:|:-----------:|:-------------:|
|        -        |     deep sleep  | Vbat = 3,61-3,66 V  | 0,020-0,021 | 0,0722-0,0769 |
|      v3.1       |     deep sleep  | Vbat = 3,61-3,66 V  |     0,034   |  0,123-0,124  |
|      v3.1       |     deep sleep  | Vusb = 5,06-5,08 V  |  15,5-15,8  |   78,4-80,3   |
|        -        |    light sleep  | Vbat = 4,15-4,16 V  |  1,94-2,02  |   7,80-8,18   |
|      v3.1       |    light sleep  | Vbat = 4,14 V       |  1,74-1,76  |   7,20-7,29   |
|      v3.1       |    light sleep  | Vusb = 5,06-5,08 V  |  17,1-17,5  |   86,5-88,9   |
|        -        | ativo, WiFi off | Vbat = 3,61-3,66 V  |  34,5-34,7  |    121-123    |
|      v3.1       | ativo, WiFi off | Vbat = 3,61-3,66 V  |  34,5-35,0  |    125-128    |
|      v3.1       | ativo, WiFi off | Vusb = 5,06-5,08 V  |  42,7-43,4  |    216-2220   |

Veja mais medidas de consumo de energia de WiPy 3 e Expansion Board v2.x/v3.x em diferentes configurações na  
[seção '11. Consumo de energia' do tutorial 'Usando Expansion Board.md'](Usando_Expansion_Board_pt-br.md).


### 3.2. Modo ativo com WiFi off x WiFi on STA x WiFi on AP

Alimentação com cabo USB e terminal REPL aberto, com medidor USB [UM25C](https://www.aliexpress.com/item/32855845265.html), 
tensão Vusb = 5,07-5,08 V.

WiPy 3 (com PyBytes off, LED RBG com heartbeat off) + Expansion Board v3.0 só com jumpers RX/TX, sem uSD.

| Expansion Board |           Modo          |   i (ma)  | P (mW)  |
|:---------------:|:-----------------------:|:---------:|:-------:|
|      v3.0       | WiFi off                | 45,5-46,0 | 231-234 |
|      v3.0       | WiFi on, STA on, AP off |  108-109  | 548-554 |
|      v3.0       | WiFi on, STA off, AP on |  110-132  | 558-671 |


### 3.3. LED RGB off x on

Alimentação com cabo USB e terminal REPL aberto, com medidor USB [UM25C](https://www.aliexpress.com/item/32855845265.html), 
tensão Vusb = 5,07-5,08 V.

WiPy 3 (com PyBytes off, WiFi off, LED RBG com heartbeat off) + Expansion Board v3.0 só com jumpers RX/TX, sem uSD.

|    Cor e intensidade do LED RGB     |         Comando        |   i (ma)  | P (mW)  |
|:-----------------------------------:|:----------------------:|:---------:|:-------:|
| Apagado                             | pycom.rgbled(0x000000) | 45,5-46,0 | 231-234 |
| Branco, brilho muito fraco          | pycom.rgbled(0x080808) | 45,7-46,0 | 232-234 |
| Branco, brilho fraco                | pycom.rgbled(0x101010) | 46,0-46,4 | 233-236 |
| Branco, brilho médio                | pycom.rgbled(0x404040) | 50,1-50,5 | 254-257 |
| Branco, brilho forte                | pycom.rgbled(0x808080) | 55,9-56,3 | 283-286 |
| Branco, brilho forte                | pycom.rgbled(0x808080) | 55,9-56,3 | 283-286 |
| Branco, brilho máximo muito forte   | pycom.rgbled(0xffffff) | 67,9-68,2 | 344-346 |
| Azul, brilho máximo muito forte     | pycom.rgbled(0x0000ff) | 50,1-50,5 | 254-257 |
| Verde, brilho máximo muito forte    | pycom.rgbled(0x00ff00) | 50,8-51,1 | 258-260 |
| Vermelho, brilho máximo muito forte | pycom.rgbled(0xff0000) | 55,5-55,9 | 281-284 |

---

(A FAZER) medição de consumo de energia para :
- ciclo de boot com gráfico;
- ciclo de deep sleep com gráfico;
- ciclo de ligh tsleep com gráfico;
- comunicação WiFi AP/STA em diferentes situações, com gráfico;
- leitura e gravação de arquivos em memória flash interna (FAT ou LittleFS) e uSD, com gráfico;
- cálculos de benchmark.



## 4. Benchmarks

Pycom MicroPython 1.20.2.rc7 [v1.11-6d01270] on 2020-05-04; WiPy with ESP32

WiPy 3 (com PyBytes off, WiFi off, LED RBG com heartbeat off) + Expansion Board v3.0 com todos mini-jumpers, sem uSD.


### 4.1. Memória RAM livre ao iniciar MicroPython

[Módulo 'gc'](https://docs.pycom.io/firmwareapi/micropython/gc/) é bem prático para ler a memória RAM livre logo após 
iniciar MicroPython : 2.560.704 bytes = 2.500,687 kB = 2,442078 MB.
```
import gc
gc.collect()
gc.mem_free()
    2560704   
```
(A FAZER) Testar com PyBytes on e/ou WiFi on e/ou LED RBG com heartbeat on.


### 4.2. Tempo e RAM gastos para importar módulos

Medindo RAM gasta para importar módulo via [módulo 'gc'](https://docs.pycom.io/firmwareapi/micropython/gc/), p. e., para 
importar o módulo 'math' :
```
import gc
gc.collect()
gc.mem_free()   # 2560912 bytes
import math
gc.collect()
gc.mem_free()   # 2560880 bytes. 2560912 - 2560880 = 32 bytes to import
```

Medindo o tempo de importar módulo via 'machine.Timer.Chrono()' (ou via 'time.ticks_us()/time.ticks_diff()' para o módulo 
'machine').

Módulos nativos (escritos em C) oficiais, embutidos ('[frozen module](http://docs.micropython.org/en/latest/reference/constrained.html)') 
no firmware oficial Pycom :

|      Importação de módulo     | RAM usada (bytes) | Tempo para 'import' (ms) |
|:------------------------------|:-----------------:|:------------------------:|
| import array                  |        32         |           0,123          |
| import binascii               |        32         |          32,56           |
| import gc                     |        16         |           0,104          |
| import machine                |        32         |          30,91           |
| import math                   |        32         |           0,108          |
| import micropython            |        32         |           0,099          |
| import network                |        32         |           0,115          |
| from network import WLAN      |        32         |           0,188          |
| from network import Bluetooth |        32         |           0,177          |
| import os                     |        16         |          32,72           |
| import pycom                  |        32         |           0,116          |
| import socket                 |        32         |          32,88           |
| import struct                 |        32         |          32,90           |
| import time                   |        32         |          32,91           |

Ou seja :
- módulos nativos embutidos ('frozen modules') no firmware gastam quase nada de RAM ao importar;
- tem um padrão, alguns módulos sempre gastam 32-33 ms para importar, outros só 0,1-0,2 ms. Pode ser que o firmware 
Pycom armazene alguns módulos em partes diferentes da memória flash local, etc.

Módulos MicroPython da comunidade, ['Statistics'](https://github.com/rcolistete/MicroPython_Statistics) e 
['MLX90615 infrared temperature sensor driver'](https://github.com/rcolistete/MicroPython_MLX90615_driver), .py na 
partição '/flash', [.mpy](https://docs.micropython.org/en/latest/reference/mpyfiles.html) na partição '/flash', 
[frozen module](http://docs.micropython.org/en/latest/reference/constrained.html) embutido em firmware Pycom compilado :

|            Módulo           | RAM usada (bytes) | Tempo para 'import' (ms) |
|:----------------------------|:-----------------:|:------------------------:|
| statistics.py               |       2.688       |          314,46          |
| statistics.mpy              |       2.256       |           76,23          |
| statistics.py frozen module |         736       |            2,323         |
| mlx90615.py                 |      11.776       |          822,0           |
| mlx90615.mpy                |      10.864       |          254,0           |
| mlx90615.py frozen module   |       1.600       |           41,32          |

Então :
- módulos nativos (escritos em C) embutidos ('frozen modules') gastam menos RAM e tempo para importar do que módulos .py embutidos;
- módulos .py frozen modules (embutidos no firmware) gastam bem menos RAM e tempo para importar do que as versões .py na partição '/flash';
- módulos .mpy (pré-compilador em bytecode via 'mpy-cross') na partição '/flash' são uma boa alternativa razoável, com pouco economia de RAM 
  mas redução sensível no tempo para importar, comparando com versões .py na partição '/flash', com a vantagem de não precisar compilar 
  firmware.

### 4.3 Velocidade de amostragem do ADC

O [submódulo 'machine.ADC'](https://docs.pycom.io/firmwareapi/pycom/machine/adc/) permite escolher a resolução (9 a 12 bits) 
e o ganho/atenuação do ADC.

Usando script ['ADC_loop_Pycom_Chrono.py'](scripts/ADC_loop_Pycom_Chrono.py) :
```
# ADC_loop_Pycom_Chrono.py
# Reads the xxPy (LoPy/LoPy4/etc) ADC and measures the time (using 'Timer.Chrono()') and speed to read samples.
import machine

def ADCloopBenchmark(nsamples=20000, nbits=12):
    adc = machine.ADC(0, bits=nbits)
    adcread = adc.channel(pin='P13')
    chrono = machine.Timer.Chrono()
    chrono.start()
    for i in range(nsamples):
        val = adcread()
    dt = chrono.read_us()
    print("%u ADC readings done after %5.3f ms" % (nsamples, dt/1e3))
    print("Mean time for each ADC reading = %4.2f us" % (dt/nsamples))
    print("ADC reading = %5.3f ksamples/s" % (1e3*nsamples/dt))
```
e variando a resolução do ADC :
```
import ADC_loop_Pycom_Chrono

ADC_loop_Pycom_Chrono.ADCloopBenchmark(nbits=9)
    20000 ADC readings done after 1044.822 ms
    Mean time for each ADC reading = 52.24 us
    ADC reading = 19.142 ksamples/s
ADC_loop_Pycom_Chrono.ADCloopBenchmark(nbits=10)
    20000 ADC readings done after 1059.196 ms
    Mean time for each ADC reading = 52.96 us
    ADC reading = 18.882 ksamples/s
ADC_loop_Pycom_Chrono.ADCloopBenchmark(nbits=11)
    20000 ADC readings done after 1059.215 ms
    Mean time for each ADC reading = 52.96 us
    ADC reading = 18.882 ksamples/s
ADC_loop_Pycom_Chrono.ADCloopBenchmark(nbits=12)
    20000 ADC readings done after 1059.191 ms
    Mean time for each ADC reading = 52.96 us
    ADC reading = 18.882 ksamples/s
```
Vemos pouca dependência da taxa de amostragem do ADC em relação à resolução : 19,14 ksamples/s para 9 bits, **18,88 ksamples/s** 
para 10 a 12 bits.


### 4.4 Velocidade de conversão do DAC

O [submódulo 'machine.DAC'](https://docs.pycom.io/firmwareapi/pycom/machine/dac/) não permite opções ao usar o DAC, que tem 
resolução fixa em 8 bits.

Usando script ['DAC_loop_Pycom_Chrono.py'](scripts/DAC_loop_Pycom_Chrono.py) :
```
# DAC_loop_Pycom_Chrono.py
# Outputs to the xxPy (LoPy/LoPy4/etc) DAC pin P22 and measures the time (using 'Timer.Chrono()') and speed.
import machine

def DACloopBenchmark(nsamples=1000, output=True):
    dac = machine.DAC('P22')
    chrono = machine.Timer.Chrono()
    chrono.start()
    for i in range(nsamples):
        dac.write(i/nsamples)
    dt = chrono.read_us()
    if output:
        print("%u DAC outputs done after %5.3f ms" % (nsamples, dt/1e3))
        print("Mean time for each DAC output = %4.2f us" % (dt/nsamples))
        print("DAC speed = %5.3f ksamples/s" % (1e3*nsamples/dt))
```
**a velocidade do DAC é 25,3 ksamples/s** :
```
import DAC_loop_Pycom_Chrono

DAC_loop_Pycom_Chrono.DACloopBenchmark()
    1000 DAC outputs done after 38.708 ms
    Mean time for each DAC output = 38.71 us
    DAC speed = 25.835 ksamples/s
DAC_loop_Pycom_Chrono.DACloopBenchmark(10000)
    10000 DAC outputs done after 395.711 ms
    Mean time for each DAC output = 39.57 us
    DAC speed = 25.271 ksamples/s
DAC_loop_Pycom_Chrono.DACloopBenchmark(50000)
    50000 DAC outputs done after 1978.959 ms
    Mean time for each DAC output = 39.58 us
    DAC speed = 25.266 ksamples/s
```
Com saída fixa, p. e., 'dac.write(0.42)', a velocidade do DAC é 46-47 ksamples/s.

Loop gerando sinal dente-de-serra (rampa) :
```
import DAC_loop_Pycom_Chrono

while True:
    DAC_loop_Pycom_Chrono.DACloopBenchmark(1e4, False)
```
Sinal de tensão dente-de-serra (rampa) no pino P22, escalas 100 ms/div e 500 mV/div no 
[osciloscópio BitScope Micro BS05](http://bitscope.com/product/BS05/) :
<img src="img/BitScope_100ms_DAC_WiPy3.png" alt="Sinal dente-de-serra gerado pelo DAC" width="800"/>


### 4.5. Frequência de chaveamento de pino digital (GPIO) de saída

Loop com 'while' infinito é o mais rápido :
```
import machine

p7_out = machine.Pin('P7', mode=machine.Pin.OUT)
while True:
    p7_out.toggle()
```
Medição via [osciloscópio FNIRSI/KKmoon 2031H](http://www.fnirsi.cn/productinfo/392305.html) : f = 35,6-36,2 kHz, T = 27,4-27,9 us.  
Medição via [osciloscópio BitScope Micro BS05](http://bitscope.com/product/BS05/) : permite ver que os períodos oscilam a cada alguns ciclos, desde 
f = 42,6 kHz, T = 23,5 us até f = 31,8 kHz, T = 31,4  us. 251 us em 10 ciclos resulta em média de **f = 39,8 kHz**, T = 25,1 us.

Loop com 'for' finito permite também medir a frequência por software :
```
import machine
chrono_all = machine.Timer.Chrono()

p7_out = machine.Pin('P7', mode=machine.Pin.OUT)
chrono_all.start()
N = 2e6
for i in range(N):
    p7_out.toggle()
dt = chrono_all.read()
print('f = {:.3f} kHz, T = {} us'.format(N/(2*1e3*dt), 2*1e6*dt/N))
```
Medição via software : **f = 26,094 kHz**, T = 38,32331 us.  
Medição via [osciloscópio FNIRSI/KKmoon 2031H](http://www.fnirsi.cn/productinfo/392305.html) : f = 25,7-26,3 kHz, T = 37,7-38,8 us.  
Medição via [osciloscópio BitScope Micro BS05](http://bitscope.com/product/BS05/) : permite ver que os períodos oscilam a cada alguns ciclos, desde 
f = 27,8 kHz, T = 36 us até f = 18,7 kHz, T = 53,4 us. 385 us em 10 ciclos resulta em média de f = 26,0 kHz, T = 38,5 us.

Onda quadrada no pino P7 tem períodos que não são iguais, escalas 50 us/div e 500 mV/div no 
[osciloscópio BitScope Micro BS05](http://bitscope.com/product/BS05/) :
<img src="img/BitScope_50us_GPIO_for_WiPy3.png" alt="Onda quadrada no pino P7" width="800"/>


### 4.6. Tempo para acordar após light sleep

Considerando média e desvio padrão dos 2 métodos abaixo, o **tempo para acordar após light sleep = (77 +/- 11) us**.

Light sleep é implementado via função ['machine.sleep([time_ms], resume_wifi_ble)'](https://docs.pycom.io/firmwareapi/pycom/machine/).

Alimentação com cabo USB e terminal REPL aberto via [rshell](https://github.com/dhylands/rshell).

'machine.sleep(1)' frequentemente gera 'CORE DUMP'. Bem como 'machine.sleep()' usando wake-up from Pin (high ou low), se já estiver 
conectado, gera 'CORE DUMP'. (A FAZER) Testar outras versões de firmware para verificar se esse bug é resolvido.

Medindo tempo de pino digital alto nos ciclos de light sleep, via software e osciloscópio no pino P9 do 'User LED' da Expansion Board, 
com o script abaixo como 'main.py' :
```
import machine
import time
N = 1e5
p_led = machine.Pin('P9', mode=machine.Pin.OUT)   # 'user LED' é ligado ao inicializar o pino G16/P9
t1 = time.ticks_us()
for i in range(N):
    time.sleep_us(1000)    # 1000 us
    machine.sleep(2)       # 2 ms light sleep, upon exit, peripherals and CPUs resume operation, their internal state is preserved
t2 = time.ticks_us()
dt = time.ticks_diff(t2, t1)
print('Total time = {} us, each loop = {} us'.format(dt, dt/N))
```
- medição via software :
   * sem 'machine.sleep()', com 'time.sleep_us(1000)', N = 1e5 : 'Total time = 103196024 us, each loop = 1031.96 us' -> menos 
     1000 us, cada loop = 31,96 us;
   * com 'machine.sleep(2)' e 'time.sleep_us(1000)', N = 1e5 : 'Total time = 312187495 us, each loop = 3121.875 us' -> menos 3000 us, 
     cada loop = 122 us. Subtraindo o tempo do 'for' & 'time.sleep_us()', 32 us, então resulta em **90 us para light sleep acordar**;
- medição via [osciloscópio FNIRSI/KKmoon 2031H](http://www.fnirsi.cn/productinfo/392305.html) :
   * com 'machine.sleep(2)' e 'time.sleep_us(1000)', N = 1e5 : f = 321,5 Hz, T+ = 1,59 ms, T- = 1,52 ms (LED aceso), T = 3,11 ms. 
     Logo 2 ms de light sleep gasta 1,59 ms com saída desligada, (1,52 - 1,0 = 0,52) ms com saída ligada, total de 2,11 ms, menos a 
     pausa de 2 ms, resulta em 0,11 ms  para cada loop sem as pausas. Subtraindo o tempo do 'for' & 'time.sleep_us()', 32 us, então 
     resulta em **0,08 ms para light sleep acordar**;
   * com 'machine.sleep(2)' e 'time.sleep_us(0)', N = 1e5 : f = 473,9 Hz, T+ = 1,59 ms, T- = 521 us (LED aceso), T = 2,11 ms.
     Logo 2 ms de light sleep gasta 1,59 ms com saída desligada, 0,521 ms com saída ligada, total de 2,11 ms, menos a 
     pausa de 2 ms, resulta em 0,11 ms  para cada loop sem as pausas. Subtraindo o tempo do 'for' & 'time.sleep_us()', 32 us, então 
     resulta em **0,08 ms para light sleep acordar**;
- medição via [osciloscópio BitScope Micro BS05](http://bitscope.com/product/BS05/) : 
   * com 'machine.sleep(2)' e 'time.sleep_us(1000)', N = 1e5 : f = 321,75 Hz, T+ = 1,576 ms, T- = 1,532 ms (LED aceso), T = 3,108 ms. 
     Logo 2 ms de light sleep gasta 1,576 ms com saída desligada, (1,532 - 1,0 = 0,532) ms com saída ligada, total de 2,108 ms, menos a 
     pausa de 2 ms, resulta em 0,108 ms = 108 us para cada loop sem as pausas. Subtraindo o tempo do 'for' & 'time.sleep_us()', 32 us, 
     então resulta em **76 us = 0,076 ms para light sleep acordar**;
   * com 'machine.sleep(2)' e 'time.sleep_us(0)', N = 1e5 : f = 478,0 Hz, T+ = 1,576 ms, T- = 516 us (LED aceso), T = 2,092 ms.
     Logo 2 ms de light sleep gasta 1,576 ms com saída desligada, 516 ms com saída ligada, total de 2,092 ms, menos a 
     pausa de 2 ms, resulta em 0,092 ms = 92 us para cada loop sem as pausas. Subtraindo o tempo do 'for' & 'time.sleep_us()', 32 us, 
     então resulta em **60 us = 0,060 ms para light sleep acordar**.


### 4.7. Tempo para acordar após deep sleep

Considerando média e desvio padrão dos 2 métodos abaixo, o **tempo para acordar após deep sleep = (0,75 +/- 0,02) s**.

Deep sleep é implementado via função ['machine.deepsleep([time_ms])'](https://docs.pycom.io/firmwareapi/pycom/machine/).

#### 4.7.1. Via software, medindo tempo do RTC nos ciclos de deep sleep

Alimentação com cabo USB e terminal REPL aberto via [rshell](https://github.com/dhylands/rshell).

Medindo o tempo gasto na inicialização de 'Timer.Chrono()' :
```
import time
t1 = time.ticks_us()
#
import machine
chrono_all = machine.Timer.Chrono()
chrono_all.start()
#
t2 = time.ticks_us()
time.ticks_diff(t2, t1) 
```
Resulta em (us) : [27523, 27552, 27561, 27487, 27551, 27505, 27551, 27552, 27542].  
Média = 27536 us, desvio padrão = 25.38 us, i. e., (27.54 +/- 0.03) ms.

Usando diferença de medidas do RTC interno do ESP32, com o script abaixo como 'main.py' :
```
import machine
chrono_all = machine.Timer.Chrono()
chrono_all.start()

import time
import pycom

pycom.heartbeat(False)
pycom.rgbled(0x004000)          # Green
rtc = machine.RTC()             # internal RTC of ESP32
print('Running time before RTC.now() : {:.3f} ms'.format(chrono_all.read()*1000))
print("RTC date hour tuple : {}".format(rtc.now()))
print('Running time after RTC.now() : {:.3f} ms'.format(chrono_all.read()*1000))
print('Total running time : {:.3f} ms'.format(chrono_all.read()*1000))
time.sleep_ms(1)                # 1 ms, time to print show on terminal
machine.deepsleep(1)            # 1 ms deep sleep, wait turning off everything but RTC, then reboot
```
Copiando do terminal as linhas de saída de 'print's, por exemplo :
```
Running time before RTC.now() : 31.396 ms
RTC date hour tuple : (1970, 1, 1, 0, 0, 4, 509921, None)
Running time after RTC.now() : 41.591 ms
Total running time : 45.425 ms

Running time before RTC.now() : 31.396 ms
RTC date hour tuple : (1970, 1, 1, 0, 0, 5, 361636, None)
Running time after RTC.now() : 41.591 ms
Total running time : 45.425 ms

Running time before RTC.now() : 31.396 ms
RTC date hour tuple : (1970, 1, 1, 0, 0, 6, 212941, None)
Running time after RTC.now() : 41.591 ms
Total running time : 45.425 ms
```
As últimas 4 linhas com 'print' gastam aprox. (3,834 + (10,195 - 3,834 = 6,361) + 3,834 + 3,834 = 17,863) ms.
Após a medida 'rtc.now()' tem 2 print's gastando 3,834 ms cada.
Descontar então da diferença das 2 medidas de RTC o tempo 'Running time after RTC.now()' (aprox. 41,591 ms), bem como o tempo antes 
de 'import time' e depois do print com 'rtc.now()', p. e. : (27,54 + 3,834 + 3,834 + 2,00 = 37,208) ms.
Então das 3 rodadas acima obtemos :
```
( 5.36163  -  4.509921 - 0.041591 - 0.037208 = 0.772910) s
( 6.212941 -  5.361636 - 0.041591 - 0.037208 = 0.772506) s
```
Calculando com mais medidas, temos média = 0,772688 s, desvio padrão = 0,000179 us, i. e., (772,7 +/- 0,2) ms.  
Devido ao tempo gasto no print com 'rtc.now()', a incerteza deve estar na faixa de unidades de 4-6 ms.  
Então : **tempo para acordar após deep sleep = (773 +/- 6) ms.**

#### 4.7.2. Via osciloscópio medindo tempo de pino digital ativado nos ciclos de deep sleep

Medindo o tempo gasto na ativação do pino do user LED da Expansion Board :
```
import time
t1 = time.ticks_us()
import machine
p_led = machine.Pin('P9', mode=machine.Pin.OUT)
t2 = time.ticks_us()
time.ticks_diff(t2, t1)
```
Resulta em (us) : [31517, 31513, 31538, 31516, 31517, 31514].  
Média = 31.519,17 us, desvio padrão = 9,37 us. Subtraindo (163 +/- 4) us, temos (31.356 +/- 13) us = (31,36 +/- 
0,01) ms.

Medindo tempo de pino digital alto nos ciclos de deep sleep, via osciloscópio no pino P9 do 'User LED' da Expansion 
Board, com o script abaixo como 'main.py' :
```
import machine
p_led = machine.Pin('P9', mode=machine.Pin.OUT)   # 'user LED' é ligado ao inicializar o pino G16/P9
import time
time.sleep_ms(200)              # 200 ms, time to print show on terminal
machine.deepsleep(1)            # 1 ms deep sleep, wait turning off everything but RTC, then reboot
```
- Medição via [osciloscópio FNIRSI/KKmoon 2031H](http://www.fnirsi.cn/productinfo/392305.html) :
f = 1,000 Hz, T+ = 800 ms, T- = 200 ms (LED aceso), T = 1000 ms. Descontando o tempo gasto nas 2 primeiras e última 
linhas acima, (31 + 1 = 32) ms, então o tempo para acordar após deep sleep = **(768 +/- 10) ms**;
- Medição via [osciloscópio BitScope Micro BS05](http://bitscope.com/product/BS05/) : 
f = 0,9960 Hz, T+ = 772 ms, T- = 232 ms (LED aceso), T = 1004 ms. Descontando o tempo gasto nas 2 primeiras e última 
linhas acima, (31 + 1 = 32) ms, então o tempo para acordar após deep sleep = **(740 +/- 4) ms**.

Medindo tempo nos ciclos de deep sleep, via osciloscópio no pino P9 do 'User LED' da Expansion Board, com o script 
abaixo como 'main.py' :
```
import machine
p_led = machine.Pin('P9', mode=machine.Pin.OUT)   # 'user LED' é ligado ao inicializar o pino G16/P9
machine.deepsleep(1)            # 1 ms deep sleep, wait turning off everything but RTC, then reboot
```
Medição via [osciloscópio BitScope Micro BS05](http://bitscope.com/product/BS05/) : 
f = f = 1,272 Hz, T+ = 784 ms, T- = 2,4 ms, T = 786 ms. 
Descontando o tempo gasto nas 2 primeiras linhas acima, (31 + 1 = 32) ms, então o tempo para acordar após deep 
sleep = **(754 +/- 4) ms**.

Medindo tempo nos ciclos de deep sleep, via osciloscópio no pino P7, com o script abaixo como 'main.py' :
```
import machine
machine.deepsleep(1)            # 1 ms deep sleep, wait turning off everything but RTC, then reboot
```
Medição via [osciloscópio BitScope Micro BS05](http://bitscope.com/product/BS05/)5 : 
f = 1,302 Hz, T+ = 608 ms, T- = 160 ms, T = 768 ms.
Descontando o tempo gasto nas 2 primeiras linhas acima, (31 + 1 = 32) ms, então o tempo para acordar após deep 
sleep = **(736 +/- 4) ms**.


### 4.8. Processamento e cálculos

#### 4.8.1. 

#### 4.8.2. 

#### 4.8.3. 



---

A FAZER benchmarks sobre :
- time e RAM gastos para importar módulo .py, .mpy e frozen module, bem como módulos padrão mais usados (machine, network, etc);
- calculations, com também firmwares com precisão dupla quando disponíveis;
- time of boot cycle (startup) using RST pin as trigger in CHA and pin out as CHB of oscilloscope;
- time to enter interrupt;
- time to read and write from internal flash (using FAT or LittleFS) e uSD;
- vazão de dados usando WiFi;
- etc.
