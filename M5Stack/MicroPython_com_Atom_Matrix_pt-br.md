# MicroPython com M5Stack Atom Matrix

#### Autor : Roberto Colistete Jr. (roberto.colistete arroba gmail.com)
##### Última atualização : 27/03/2020.

<img src="https://docs.m5stack.com/assets/img/product_pics/core/minicore/atom/atom_matrix_01.webp" alt="Atom Matrix" width="300"/>

Atom Matrix, US$8.95, modelo de 01/2020 :  
[Atom Matrix Docs](https://docs.m5stack.com/#/en/core/atom_matrix)  
[Atom Matrix @ M5Stack Store](https://m5stack.com/products/atom-matrix-esp32-development-kit)  
[Atom Matrix  @ M5Stack AliExpress Store](https://www.aliexpress.com/item/4000576830752.html)  
ESP32 520KB SRAM, 4MB flash, Matrix 5x5 RGB LED's, MPU6886 acell/gyro, 1 user button, 1 power/reset button, IR transmitter, 
USB-C (power/serial), Grove connector, 9 female headers, gray case, 24 mm x 24 mm x 14 mm, 14 g.

Ligar pressionando o botão lateral.  
Pressionar por 6s o botão lateral para desligar.  

Nos códigos-fonte de testes :
- digite tecla <Tab> após '.' em final de linha para ver a lista de atributos do objeto;
- saídas/resultados estão indentados com 1 tab a mais.


## Opções de firmware MicroPython para Atom Matrix :

1. [UIFlow 1.4.5.1](#1-uiflow-1451-) é baseado em MicroPython 1.11 (29/05/2019) oficial para ESP32, com grande personalização da M5Stack, incluindo 
partes de MicroPython ESP32 psRAM LoBo. Reconhece quase todo o hardware, exceto transmissor IR. Tem vários drivers para as 'Units'. 
Mas : os drivers na maioria têm recursos simplificados; usa FAT, o código-fonte é fechado; a documentação de meados de 2019 para cá é incompleta; 
poucos softwares funcionam para copiar arquivos de/para a memória flash interna;

2. [MicroPython oficial v1.12 (12/2019) ou mais recente](#2-micropython-oficial-v112-ou-mais-recente), com código-fonte aberto, mais RAM livre, 
menor consumo de energia e sistema de arquivos LittleFS na flas interna. Mas tem que instalar manualmente drivers de terceiros para para os vários 
dispositivos em 'Units'.


## 1. UIFlow 1.4.5.1 :

UIFlow >= 1.3.2 é baseado em Micropython oficial, no caso UIFlow 1.4.5.1 usa 
[Micropython v1.11 oficial de 29/05/2019](https://github.com/micropython/micropython/releases/tag/v1.11) com extensas personalizações da M5Stack, 
incluindo partes (ADC, DAC, I2C, etc) de [MicroPython_ESP32_psRAM_LoBo](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/dac).  
Reconhece o RGB LED e botão. Tem vários drivers para as 'Units'. Mas :
- não tem driver/módulo para transmissor IR;
- os drivers na maioria têm recursos simplificados; 
- usa FAT como sistema de arquivos para memória flash interna, resultando em menor vida útil (umas 10 mil) de gravações;
- o código-fonte é fechado, sendo uma mistura não desconhecida de 2 MicroPython para ESP32 : oficial e Lobo;
- a documentação das personalizações da M5Stack é de meados de 2019 para cá e incompleta;
- poucos softwares funcionam para copiar arquivos de/para a memória flash interna com UIFlow 1.4.5/1.4.5.1.

Vide guia [Atom UIFlow Quick Start](https://docs.m5stack.com/#/en/quick_start/atom/atom_quick_start), para :
- usar modo 'YELLOW Light Configure WIFI' para configurar conexão na rede Wifi local (ligue apertando botão frontal até aparecer luz amarela). 
Para tanto, antes é ativado modo AP (Access Point), conecte na rede WiFi 'M5Stack-XXXX', abra navegador web no endereço '192.168.4.1', 
escolha/digite o SSID da rede WiFi local e depois digite a senha WiFi. Copie a API key (também visível via terminal REPL) pois será usada no 
UIFlow online;
- estando no modo 'GREEN Light UIFlow Online Program', abra o [site UIFlow online](http://flow.m5stack.com/), em 'Setting', no 1o campo 'Api key' 
digite o código copiado antes;
- usar modo 'BLUE Light UIFlow desktop IDE Program' (ligue apertando botão frontal até aparecer luz azul) para se 
[comunicar via cabo USB-C com o UIFlow-Desktop-IDE](https://docs.m5stack.com/#/en/uiflow/introduction?id=uiflow-ide);
- usar modo 'PURPLE Light APP Run Mode' (ligue apertando botão frontal até aparecer luz violeta) para conectar via terminal REPL mais 
facilmente.

Poucos softwares funcionam no Linux para copiar arquivos de/para a memória flash interna do Atom Lite/Matrix com UIFlow 1.4.5/1.4.5.1 :
* [mpfshell](https://github.com/wendlers/mpfshell);
* [uPyLoader](https://github.com/BetaRavener/uPyLoader). Funciona via executável para Linux. Permite copiar arquivos, mas não diretórios (nem entra).
* [Thonny](https://thonny.org/) tem [modo MicroPython](https://github.com/thonny/thonny/wiki/MicroPython), com modo "View->File" que permite copiar 
arquivos e diretórios, editar diretamente, etc, na memória flash de MicroPython. Mas há instabilidade na cópia de arquivos;

Enquanto que 'rshell', 'ampy' (erro '`ampy.pyboard.PyboardError: could not enter raw repl`'), Mu Editor, Atom/VSCode/PyCharm, uPyCraft, etc, não conectam. 
Mas 'screen' funciona.


### 1.1 Instalando firmware :

Via terminal, apagando flash via ['esptool'](https://pypi.org/project/esptool/) :  
`$ pip install esptool`  
`$ esptool.py -p /dev/ttyUSB0 -b 115200 --chip esp32 erase_flash`  
e instalando firmware UIFlow, dentro de 'UIFlow-Desktop-IDE_Linux_v1.4.5.1/assets/firmwares/UIFlow-v1.4.5.1/UIFlow-Firmware-1.4.5.1/firmware_ATOM/' :  
`$ ./flash`

Recomendado fazer backup do firmware que vem pré-instalado, de fábrica, vide [documentação de esptool](https://github.com/espressif/esptool) e 
[tutorial sobre backup/restore](https://steve.fi/hardware/backup-and-restore/).  
Backup :  
`$ esptool.py -c esp32 -p /dev/ttyUSB0 -b 115200 read_flash 0x00000 0x400000 backup.img`  
Restore :  
`$ esptool.py -c esp32 -p /dev/ttyUSB0 -b 115200 write_flash -fm dio 0x00000 backup.img`  


### 1.2 Testes e exemplos :

- veio de fábrica com 'MicroPython v1.12-36-gf68fe753b-dirty on 2020-01-14", que permitia usar ['rshell'](https://github.com/dhylands/rshell) (mais fácil) 
ou ['ampy'](https://github.com/scientifichackers/ampy) :
```
$ rshell -p /dev/ttyUSB0
> ls -l /pyboard
   139 Jan  1 2000  boot.py
> cat /pyboard/boot.py
    # This file is executed on every boot (including wake-boot from deepsleep)
    #import esp
    #esp.osdebug(None)
    #import webrepl
    #webrepl.start()
> repl
MicroPython v1.12-36-gf68fe753b-dirty on 2020-01-14; ESP32 module with ESP32
import gc
gc.collect()
gc.mem_free()
    78176
```
Tem só 1 arquivo em '/pyboard', 'boot.py' (vazio, só com comentários e comandos que podem ser habilitados, mas não tem módulo 'webrepl'). Ou seja, a 
sequência de modos de boot está congelados no firmware UIFLow/MicroPython do Atom, não é mais explícito nos arquivos 'boot.py' e 'main.py'.  
Tal versão é incompleta, não tendo vários recursos de UIFlow (LED RGB, drivers, etc).

- com UIFlow 1.4.5.1, dando reset apertando uma vez o botão laterial no modo 'PURPLE Light Run the Last Downloaded Program' :
```
I (33) boot: ESP-IDF v3.3-beta1-696-gc4c54ce07 2nd stage bootloader
I (33) boot: compile time 20:58:45
I (33) boot: Enabling RNG early entropy source...
I (39) boot: SPI Speed      : 40MHz
I (44) boot: SPI Mode       : DIO
I (48) boot: SPI Flash Size : 4MB
I (52) boot: Partition Table:
I (55) boot: ## Label            Usage          Type ST Offset   Length
I (62) boot:  0 nvs              WiFi data        01 02 00009000 00006000
I (70) boot:  1 phy_init         RF data          01 01 0000f000 00001000
I (77) boot:  2 factory          factory app      00 00 00010000 001f0000
I (85) boot:  3 internalfs       Unknown data     01 81 00200000 001ff000
I (92) boot: End of partition table
I (97) esp_image: segment 0: paddr=0x00010020 vaddr=0x3f400020 size=0xe903c (954428) map
I (436) esp_image: segment 1: paddr=0x000f9064 vaddr=0x3ffbdb60 size=0x034dc ( 13532) load
I (442) esp_image: segment 2: paddr=0x000fc548 vaddr=0x40080000 size=0x00400 (  1024) load
I (443) esp_image: segment 3: paddr=0x000fc950 vaddr=0x40080400 size=0x036c0 ( 14016) load
I (457) esp_image: segment 4: paddr=0x00100018 vaddr=0x400d0018 size=0xdbe8c (900748) map
I (773) esp_image: segment 5: paddr=0x001dbeac vaddr=0x40083ac0 size=0x12528 ( 75048) load
I (803) esp_image: segment 6: paddr=0x001ee3dc vaddr=0x400c0000 size=0x00064 (   100) load
I (803) esp_image: segment 7: paddr=0x001ee448 vaddr=0x50000000 size=0x00808 (  2056) load
I (824) boot: Loaded app from partition at offset 0x10000
I (824) boot: Disabling RNG early entropy source...
I (824) cpu_start: Pro cpu up.
I (828) cpu_start: Application information:
I (832) cpu_start: Compile time:     Mar 12 2020 20:58:50
I (839) cpu_start: ELF file SHA256:  0000000000000000...
I (845) cpu_start: ESP-IDF:          v3.3-beta1-696-gc4c54ce07
I (851) cpu_start: Starting app cpu, entry point is 0x400836e4
I (0) cpu_start: App cpu up.
I (862) heap_init: Initializing. RAM available for dynamic allocation:
I (868) heap_init: At 3FFAE6E0 len 0000F480 (61 KiB): DRAM
I (875) heap_init: At 3FFCB2C8 len 00014D38 (83 KiB): DRAM
I (881) heap_init: At 3FFE0440 len 00003AE0 (14 KiB): D/IRAM
I (887) heap_init: At 3FFE4350 len 0001BCB0 (111 KiB): D/IRAM
I (893) heap_init: At 40095FE8 len 0000A018 (40 KiB): IRAM
I (900) cpu_start: Pro cpu start user code
I (23) cpu_start: Starting scheduler on PRO CPU.
I (0) cpu_start: Starting scheduler on APP CPU.

Internal FS (FatFS): Mounted on partition 'internalfs' [size: 2093056; Flash address: 0x200000]
----------------
Filesystem size: 2031616 B
           Used: 45056 B
           Free: 1986560 B
----------------
[ M5 ] node id:d8a01d5a3c54, api key:E8200E74
-- start uiflow from app mode --
app
MicroPython v1.11-301-gd0880874d-dirty on 2020-03-13; ESP32 module with ESP32
Type "help()" for more information.
```
no modo 'YELLOW Light Configure WIFI' só o final muda :
```
-- start uiflow from internet mode --
I (1993) phy: phy_version: 4100, 2a5dd04, Jan 23 2019, 21:00:07, 0, 0
WiFi AP WebServer Start!
Connect to Wifi SSID:M5Stack-3c54
And connect to esp via your web browser (like 192.168.4.1)
listening on ('0.0.0.0', 80)
```
no modo 'BLUE Light UIFlow desktop IDE Program' :
```
-- start uiflow from USB mode --
start m5ucloud monitor

I (8108) m5uart: Creat m5uart task
```
no modo 'GREEN Light UIFlow Online Program' :
```
-- start uiflow from internet mode --
I (2021) phy: phy_version: 4100, 2a5dd04, Jan 23 2019, 21:00:07, 0, 0
Connect Wi-Fi: SSID:NautilusCasa PASSWD:####### network...

Connected. Network config: ('192.168.1.107', '255.255.255.0', '192.168.1.1', '187.36.192.43')
M5Cloud connected.
m5cloud thread begin .....
```

- com 'boot.py' e 'main.py' de 'UIFlow_v1.45.1 - Base Micropython v1.11' :
```
$ screen /dev/ttyUSB0 115200
MicroPython v1.11-321-gac7da0a70-dirty on 2020-02-25; ESP32 module with ESP32  
import gc
gc.collect()
gc.mem_free()
    78592   # 76.8 kB
```

- módulo 'os' em MicroPython v1.11 não suporta LittleFS, então a memória flash interna vem formatada com FAT (ao menos até MicroPython <= 1.12).
```
import os
os.
    __class__       __name__        remove          SDMODE_1LINE
    SDMODE_4LINE    SDMODE_SPI      chdir           dupterm
    dupterm_notify  getcwd          getdrive        ilistdir
    listdir         mkdir           mountsd         rename
    rmdir           sdconfig        stat            statvfs
    umountsd        uname           urandom
os.uname()   # versão de MicroPython
    (sysname='esp32', nodename='esp32', release='1.11.0', version='v1.11-301-gd0880874d-dirty on 2020-03-13', machine='ESP32 module with ESP32')
```

- [módulo 'machine'](https://docs.micropython.org/en/latest/library/machine.html) e frequência do ESP32 (240 MHz, não dá para alterar) :
```
import machine
machine.freq()
    240000000
```

- help :
```
help()
    Welcome to MicroPython on the ESP32!
    
    For generic online docs please visit http://docs.micropython.org/
    
    For access to the hardware use the 'machine' module:
    
    import machine
    pin12 = machine.Pin(12, machine.Pin.OUT)
    pin12.value(1)
    pin13 = machine.Pin(13, machine.Pin.IN, machine.Pin.PULL_UP)
    print(pin13.value())
    i2c = machine.I2C(scl=machine.Pin(21), sda=machine.Pin(22))
    i2c.scan()
    i2c.writeto(addr, b'1234')
    i2c.readfrom(addr, 4)
    
    Basic WiFi configuration:
    
    import network
    sta_if = network.WLAN(network.STA_IF); sta_if.active(True)
    sta_if.scan()                             # Scan for available access points
    sta_if.connect("<AP_name>", "<password>") # Connect to an AP
    sta_if.isconnected()                      # Check for successful connection
    
    Control commands:
      CTRL-A        -- on a blank line, enter raw REPL mode
      CTRL-B        -- on a blank line, enter normal REPL mode
      CTRL-C        -- interrupt a running program
      CTRL-D        -- on a blank line, do a soft reset of the board
      CTRL-E        -- on a blank line, enter paste mode
    
    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (drivers, etc, do UIFlow MicroPython firmware) :
```
    help('modules')
    __main__          flowlib/lib/time_ex                 flowlib/units/_earth                mlx90640
    _onewire          flowlib/lib/urequests               flowlib/units/_env                  neopixel
    _thread           flowlib/lib/wave  flowlib/units/_ext_io               network
    array             flowlib/lib/wavplayer               flowlib/units/_finger               os
    binascii          flowlib/lib/wifiCardKB              flowlib/units/_gps                  random
    btree             flowlib/lib/wifiCfg                 flowlib/units/_ir re
    builtins          flowlib/lib/wifiWebCfg              flowlib/units/_joystick             select
    cmath             flowlib/m5cloud   flowlib/units/_light                socket
    collections       flowlib/m5mqtt    flowlib/units/_makey                ssl
    display           flowlib/m5stack   flowlib/units/_mlx90640             struct
    errno             flowlib/m5ucloud  flowlib/units/_ncir                 sys
    esp               flowlib/module    flowlib/units/_pahub                time
    esp32             flowlib/modules/_baseX              flowlib/units/_pbhub                ubinascii
    espnow            flowlib/modules/_cellular           flowlib/units/_pir                  ucollections
    flowlib/app_manage                  flowlib/modules/_goPlus             flowlib/units/_relay                ucryptolib
    flowlib/button    flowlib/modules/_lego               flowlib/units/_rfid                 uctypes
    flowlib/face      flowlib/modules/_legoBoard          flowlib/units/_rgb                  uerrno
    flowlib/faces/_calc                 flowlib/modules/_lidarBot           flowlib/units/_rgb_multi            uhashlib
    flowlib/faces/_encode               flowlib/modules/_lorawan            flowlib/units/_servo                uhashlib
    flowlib/faces/_finger               flowlib/modules/_m5bala             flowlib/units/_tof                  uheapq
    flowlib/faces/_gameboy              flowlib/modules/_plus               flowlib/units/_tracker              uio
    flowlib/faces/_joystick             flowlib/modules/_pm25               flowlib/units/_weight               ujson
    flowlib/faces/_keyboard             flowlib/modules/_servo              flowlib/utils     uos
    flowlib/faces/_rfid                 flowlib/modules/_stepMotor          gc                upip
    flowlib/flowSetup flowlib/peripheral                  hashlib           upip_utarfile
    flowlib/i2c_bus   flowlib/power     heapq             urandom
    flowlib/lib/bmm150                  flowlib/remote    io                ure
    flowlib/lib/bmp280                  flowlib/simple    json              uselect
    flowlib/lib/chunk flowlib/timeSchedule                lidar             usocket
    flowlib/lib/dht12 flowlib/uiflow    logging           ussl
    flowlib/lib/easyIO                  flowlib/unit      m5base            ustruct
    flowlib/lib/emoji flowlib/units/_accel                m5uart            utime
    flowlib/lib/imu   flowlib/units/_adc                  m5ui              utimeq
    flowlib/lib/mpu6050                 flowlib/units/_angle                machine           uwebsocket
    flowlib/lib/mstate                  flowlib/units/_button               math              uzlib
    flowlib/lib/numbers                 flowlib/units/_cardKB               microWebSocket    zlib
    flowlib/lib/pid   flowlib/units/_color                microWebSrv
    flowlib/lib/sh200q                  flowlib/units/_dac                  microWebTemplate
    flowlib/lib/speak flowlib/units/_dual_button          micropython
    Plus any modules on the filesystem
```

- [módulo 'esp'](https://docs.micropython.org/en/latest/library/esp.html) e tamanho da memória flash interna (4 MB) :
```
import esp
esp.
    __class__       __name__        LOG_DEBUG       LOG_ERROR
    LOG_INFO        LOG_NONE        LOG_VERBOSE     LOG_WARNING
    dht_readinto    flash_erase     flash_read      flash_size
    flash_user_start                flash_write     gpio_matrix_in
    gpio_matrix_out                 neopixel_write  osdebug
esp.flash_size()
    4194304   # 4MB
```

- [módulo 'esp32'](https://docs.micropython.org/en/latest/library/esp32.html), temperatura e leitura do sensor Hall do ESP32 :
```
import esp32
esp32.
    __class__       __name__        ULP             WAKEUP_ALL_LOW
    WAKEUP_ANY_HIGH                 hall_sensor     raw_temperature
    wake_on_ext0    wake_on_ext1    wake_on_touch
esp32.raw_temperature()   # in F
    128
(esp32.raw_temperature() - 32)/1.8   # in C
    53.33333
esp32.hall_sensor()
    8
```

- módulo 'm5stack' :
```
import m5stack
m5stack.
    __class__       __name__        const           __file__
    binascii        display         m5base          machine
    os              __VERSION__     apikey          node_id
    rgb             btnA            BtnChild        Btn
    btn             timEx           lcd             hwDeinit
    timerSch        Rgb_multi       time_ex         timeSchedule
```

- [módulo 'm5ui'](https://github.com/m5stack/UIFlow-Code/wiki/M5UI) :
```
import m5ui
m5ui.
    __class__       M5Button        M5Circle        M5Img
    M5Rect          M5TextBox       M5Title         M5UI_Deinit
    setScreenColor
```

- módulo 'uiflow' :
```
import uiflow
uiflow.
    __class__       __name__        const           start
    __file__        binascii        display         gc
    m5base          machine         os              time
    wait            apikey          serverChoose    node_id
    loopExit        rgb             loopSetIdle     loopState
    btnA            flowDeinit      flowExit        BtnChild
    Btn             btn             wait_ms         timEx
    lcd             config_normal   cfgWrite        cfgRead
    modeSet         core_start      resetDefault    startBeep
    _p2pData        _nextP2PTime    sendP2PData     setP2PData
    getP2PData      _exitState      _is_remote      remoteInit
    hwDeinit        timerSch        Rgb_multi       time_ex
    timeSchedule
```

- LED RGB via submódulo 'm5stack.rgb' [não é bem documentado em UIFlow](https://github.com/m5stack/UIFlow-Code/wiki/Unit#neopixel), 
mas UIFlow-Desktop-IDE ou online permite ver exemplos simples em Blockly->MicroPython. Notar que o submódulo 'm5stack.rgb.np' é o 
[módulo neopixel do firmware MicroPython_ESP32_psRAM_LoBo](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/neopixel).
```
from m5stack import rgb
rgb.
    __class__       __init__        __module__      __qualname__
    __dict__        brightness      deinit          pin
    setBrightness   setColor        show            port
    setColorAll     brightnessUpdate                setShowLock
    number          updateTime      setColorFrom    set_screen
    np              show_lock       updateDir
rgb.np.
    __class__       clear           get             set
    BLACK           BLUE            CYAN            GRAY
    GREEN           HSBtoRGB        HSBtoRGBint     LIME
    MAGENTA         MAROON          NAVY            OLIVE
    PURPLE          RED             RGBtoHSB        SILVER
    TEAL            TYPE_RGB        TYPE_RGBW       WHITE
    YELLOW          brightness      color_order     deinit
    info            rainbow         setHSB          setHSBint
    setWhite        show            timings
rgb.setBrightness(10)       # 0-255
rgb.setColorAll(0xff0000)   # vermelho
rgb.setShowLock(True)       # não atualiza as mudanças de brilho e cor no RGB LED
rgb.setBrightness(255)      # só define
rgb.setColor(1, 0x00ff00)   # só define
rgb.show                    # agora exibe as mudanças
rgb.np.set(1, rgb.np.BLUE)  # via neopixel submódulo, azul
rgb.np.get(1)               # 
    255
```

- [botão A](https://github.com/m5stack/UIFlow-Code/wiki/Event#button) (com algumas diferenças) :
```
from m5stack import btnA
btnA.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          pin             wasPressed
    wasDoublePress  pressFor        wasReleased     isPressed
    isReleased      restart         upDate          _event
    _eventLast      _valueLast      _pressTime      _releaseTime
    _doubleTime     _dbtime         _holdTime       _cbState
    _eventTime      cb
btnA.isPressed()    # sem pressionar o botão frontal, botão A
    False
btnA.isPressed()    # pressionando o botão frontal, botão A 
    True
btnA.wasPressed(lambda: print('button A pressed'))               # ativa interrupção ao pressionar o botão A, mostrando texto no terminal
btnA.wasReleased(lambda: print('button A released'))             # ativa interrupção ao liberar o botão A, mostrando texto no terminal
btnA.wasDoublePress(lambda: print('button A double released'))   # ativa interrupção ao pressionar duas vezes o botão A, mostrando texto no terminal
```

- transmissor IR está conectado ao pino G12 (não tem diagrama, mas parece que alimenta na parte positiva do LED IR), 
[vide documentação do Atom Matrix](https://docs.m5stack.com/#/en/core/atom_matrix) :
```
from machine import Pin
ir = Pin(12, Pin.OUT)
ir.
    __class__       value           IN              INOUT
    IRQ_FALLING     IRQ_RISING      OPEN_DRAIN      OUT
    OUT_OD          PULL_DOWN       PULL_FLOAT      PULL_HOLD
    PULL_UP         WAKE_HIGH       WAKE_LOW        deinit
    init            irq             off             on
ir.value()    # IR desligado
    0
ir.value(1)   # IR ligado
ir.off()      # IR desligado
ir.on()       # IR ligado
```

- acelerômetro/giroscópio MPU6886, não é documentado em UIFlow, mas UIFlow-Desktop-IDE ou online permite ver exemplos
simples em Blockly->MicroPython :
```
import imu
imu.
    __class__       __name__        __file__        i2c_bus
    mpu6050         sh200q          SH200Q_ADDR     MPU6050_ADDR
    imu_i2c         IMU
imu0 = imu.IMU()
imu0.
    __class__       __enter__       __exit__        __init__
    __module__      __qualname__    __dict__        address
    i2c             _register_char  acceleration    gyro
    ypr             _accel_fs       _gyro_fs        preInterval
    accCoef         gyroCoef        angleGyroX      angleGyroY
    angleGyroZ      angleX          angleZ          angleY
    gyroXoffset     gyroYoffset     gyroZoffset     _accel_so
    _gyro_so        _register_short                 setGyroOffsets
    whoami          _register_three_shorts          _accel_sf
    _gyro_sf
imu0.whoami
    25
imu0.acceleration
    (-0.036, 0.036, 1.041)   # in g, M5Stick na horizontal
imu0.gyro
    (1.542, 0.229, 1.023)
```

- RTC nativo de MicroPython oficial para ESP32, vide [exemplo](http://docs.micropython.org/en/v1.10/esp32/quickref.html#real-time-clock-rtc), 
sendo que não segue o padarão de [RTC do MicroPython oficial](https://docs.micropython.org/en/latest/library/machine.RTC.html), não tendo 
alarme, etc.  
'RTC.datetime()' precisa ter 8 argumentos que não são bem documentados, mas vide essa 
[GitHub issue 'RTC cannot be set correctly on ESP 32 #4540'](https://github.com/micropython/micropython/issues/4540#issuecomment-580965520),
notando que dia da semana (0-6 começando com 2a-feira) é desprezado pois é calculado automaticamente.  
**O RTC nativo do ESP32 não mantém hora e data se o ESP32 for desligado** (apertando o botão lateral para desligar o Atom Matrix).
```
from machine import RTC
rtc = RTC()
rtc.
    __class__       datetime        init            memory
rtc.datetime()
    (2000, 1, 1, 5, 0, 15, 23, 883318)
rtc.datetime((2020, 3, 24, 0, 13, 11, 20, 0))   # (year, month, day, day of week, hour, minute, second, microsecond, tzinfo)
```

- [módulo 'i2c_bus'](https://github.com/m5stack/UIFlow-Code/wiki/Advanced#i2c) de UIFlow, com barramento I2C listando nenhum dispositivo :
```
import i2c_bus
i2c = i2c_bus.get(i2c_bus.M_BUS)
i2c.
    __class__       end             start           stop
    CBTYPE_ADDR     CBTYPE_NONE     CBTYPE_RXDATA   CBTYPE_TXDATA
    MASTER          READ            SLAVE           WRITE
    address         begin           callback        clock_timing
    data_timing     deinit          getdata         init
    is_ready        read_byte       read_bytes      readfrom
    readfrom_into   readfrom_mem    readfrom_mem_into
    resetbusy       scan            setdata         start_timing
    stop_timing     timeout         write_byte      write_bytes
    writeto         writeto_mem
i2c
    I2C (Port=1, Mode=MASTER, Speed=100000 Hz, sda=25, scl=21)
i2c.scan()
    []   # no I2C devices
```

- também se pode inicializar I2C via ['machine.I2C'](https://docs.micropython.org/en/latest/esp32/quickref.html#i2c-bus), forma 
padrão do MicroPython para ESP32. Com barramento I2C listando nenhum dispositivo :
```
from machine import I2C
i2c = I2C(freq=400000, sda=25, scl=21)
i2c.
    __class__       end             start           stop
    CBTYPE_ADDR     CBTYPE_NONE     CBTYPE_RXDATA   CBTYPE_TXDATA
    MASTER          READ            SLAVE           WRITE
    address         begin           callback        clock_timing
    data_timing     deinit          getdata         init
    is_ready        read_byte       read_bytes      readfrom
    readfrom_into   readfrom_mem    readfrom_mem_into
    resetbusy       scan            setdata         start_timing
    stop_timing     timeout         write_byte      write_bytes
    writeto         writeto_mem
i2c
    I2C (Port=0, Mode=MASTER, Speed=400000 Hz, sda=25, scl=21)
i2c.scan()
    []   # no I2C devices
```

<img src="https://docs.m5stack.com/assets/img/product_pics/unit/tof/unit_tof_01.jpg" alt="M5Stack Unit ToF VL53L0X" width="300"/>

- [Unit TOF VL53L0X](https://docs.m5stack.com/#/en/unit/tof) com driver UIFLow, [exemplos](https://github.com/m5stack/UIFlow-Code/wiki/Unit#tof) e 
[código-fonte de meados de 2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_tof.py), mostra 70 atualizações/leituras por segundo.  
Seria interessante a alternativa de usar driver mais completo para VL53L0X, permitindo configurar e acessar todos os recursos do sensor 
['VL53L0X Time-of-Flight (ToF) ranging sensor'](https://www.st.com/en/imaging-and-photonics-solutions/vl53l0x.html), por exemplo :
  1. range profile 'default mode' : maximum rane 120 cm, range timing budget 30 ms, accuracy < 4%;
  2. range profile 'high accuracy' : maximum rane 120 cm, range timing budget 200 ms, accuracy < 3%;
  3. range profile 'long range' : maximum rane 200 cm, range timing budget 33 ms, accuracy < 4%;
  4. range profile 'high speed' : maximum rane 120 cm, range timing budget 20 ms, accuracy < 5%;
  5. 'single ranging' mode : leitura só quando função é chamada;
  6. 'continuous ranging' mode : leitura em modo contínuo, lendo novamente logo que leitura anterior acaba;
  7. 'continuous timed ranging' mode : leitura em modo contínuo, lendo novamente um intervalo de tempo após leitura anterior acabar;  

O driver UIFlow para Unit TOF VL53L0X adota possivelmente (1) (pois lê até 1200-1300 mm) e (6) pois a função 'distance()' executa muito rápido.
```
import time
import unit
unit_tof = unit.get(unit.TOF, unit.PORTA)
unit_tof.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          distance        timer
    state           i2c             _available      _register_short
    _register_char  _update
unit_tof.i2c
    I2C (Port=0, Mode=MASTER, Speed=100000 Hz, sda=26, scl=32)   # different I2C bus, specific for the Grove connector
unit_tof.i2c.scan()
    [41]        #  VL53L0X default I2C address is 0x29 = 41
unit_tof.distance
    115   # mm
while True:
    ti = time.ticks_us()
    d = unit_tof.distance
    tf = time.ticks_us()
    dt = (tf - ti)
    print('d = {} mm, dt = {} ms'.format(d, dt/1000))   # d = 181 mm, dt = 0.071 ms
```

- [Unit TOF VL53L0X](https://docs.m5stack.com/#/en/unit/tof) com drive externo. O barramento I2C no conector Grove do Atom Lite usa 
sda=G26 e scl=G32, [vide documentação do Atom Lite](https://docs.m5stack.com/#/en/core/atom_lite).  
O [driver MicroPython para VL53L0X de Radomir Dopieralski](https://bitbucket.org/thesheep/micropython-vl53l0x/src/default/) é mais 
completo que o driver UIFlow/MicroPython em termos de acesso aos recursos do 
['VL53L0X Time-of-Flight (ToF) ranging sensor'](https://www.st.com/en/imaging-and-photonics-solutions/vl53l0x.html), por exemplo :
  1. range profile 'default mode' : maximum rane 120 cm, range timing budget 30 ms, accuracy < 4%;
  2. range profile 'high accuracy' : maximum rane 120 cm, range timing budget 200 ms, accuracy < 3%;
  3. range profile 'long range' : maximum rane 200 cm, range timing budget 33 ms, accuracy < 4%;
  4. range profile 'high speed' : maximum rane 120 cm, range timing budget 20 ms, accuracy < 5%;
  5. 'single ranging' mode : leitura só quando função é chamada;
  6. 'continuous ranging' mode : leitura em modo contínuo, lendo novamente logo que leitura anterior acaba;
  7. 'continuous timed ranging' mode : leitura em modo contínuo, lendo novamente um intervalo de tempo após leitura anterior acabar;  

O driver adota possivelmente (1) como default (não é configurável); (5) via função 'read()'; (6-7) via 'start(period=0)' (('period' é 
em ms mas parece que tem um bug quando diferente de 0 resultando em 'TimeoutError:') e 'stop()' com leitura via 'read()' consomido 20 mA.
```
import time
import vl53l0x
from machine import I2C, Pin
i2c = I2C(freq=400000, sda=26, scl=32)
i2c.scan()
    [41]        #  VL53L0X default I2C address is 0x29 = 41
vl53l0x.
    __class__       __name__        const           __file__
    TimeoutError    ustruct         utime           _IO_TIMEOUT
    VL53L0X
tof = vl53l0x.VL53L0X(i2c)
tof.
    __class__       __init__        __module__      __qualname__
    read            start           stop            __dict__
    init            i2c             address         _started
    _registers      _register       _flag           _config
    _stop_variable  _spad_info      _calibrate
while True:
    print(tof.read())
    time.sleep_ms(200)
# Usando 'single ranging' mode (default), 'read()' espera pela medida ser feita
while True:
    ti = time.ticks_us()
    d = tof.read()
    tf = time.ticks_us()
    dt = (tf - ti)
    print('d = {} mm, dt = {} ms'.format(d, dt/1000))   # d = 93 mm, dt = 35.848 ms
# Usando 'continuous ranging' mode, as medidas estão sendo feitas continuamente em background pelo sensor, se a medida já estiver 
# feita então 'read()' não espera :
tof.start()
while True:
    ti = time.ticks_us()
    d = tof.read()
    tf = time.ticks_us()
    dt = (tf - ti)
    print('d = {} mm, dt = {} ms'.format(d, dt/1000))   # d = 96 mm, dt = 2.256 ms
    time.sleep_ms(35)
```

- [Unit ADC 16 bits ADS1100](https://docs.m5stack.com/#/en/unit/adc), [exemplos](https://github.com/m5stack/UIFlow-Code/wiki/Unit#adc) e 
[código-fonte de meados de 2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_adc.py), onde se vê que as opções 
(mode, rate, gain, etc) não estão implementadas nesse código-fonte.  
Default : offset = 0.25V, mode = continue (or single shot), rate = 15 samples/s (or 30/60/240), gain = 1x (or 2x/4x/8x). 
Tais valores discordam do [datasheet do ADS1100](https://www.ti.com/product/ADS1100), que cita 8/16/32/128 samples/s com resolução 
respectiva de 12/14/15/16 bits, default é 8 samples/s e resolução de 12 bits.  
Seria interessante a alternativa de usar driver mais completo para ADS1100, permitindo configurar e acessar todos os recursos desse sensor.
```
import unit
unit_adc = unit.get(unit.ADC, unit.PORTA)
unit_adc.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          mode            offset
    rate            i2c             _available      _write_u8
    _read_u16       measure_set     voltage         gain
    mini_code       gain_code
unit_adc.i2c
    I2C (Port=0, Mode=MASTER, Speed=100000 Hz, sda=26, scl=32)   # different I2C bus, specific for the Grove connector
 unit_adc.i2c.scan()
    [72]           # ADS1100 default I2C address is 0x48 = 72
unit_adc.voltage   # get voltage, value range 0~12V
    3.271          # 3V3 (3.377 V on voltimeter)
    4.618          # 5V (4.766 V on voltimeter)
```

- ADC nativo do ESP32 é de 12 bits, em MicroPython limitado aos pinos 32 a 39, sendo que no [Atom Matrix](https://docs.m5stack.com/#/en/core/atom_matrix) 
o pino G33 está exposto no barramento superior de 9 pinos e o pino G32 está no conector Grove.  
Vide [documentação oficial de MicroPython para ESP32 usando ADC](https://docs.micropython.org/en/latest/esp32/quickref.html#adc-analog-to-digital-conversion).  
Mas ao que parece UIFlow usou partes de "MicroPython_ESP32_psRAM_LoBo" para implementar mais funções no ADC para ESP32, vide 
[documentação de ADC para MicroPython_ESP32_psRAM_LoBo](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/adc).  
Notar que **o ADC do ESP32 tem diversos problemas** : offset (60-130 mV), não-linearidade e alto ruído (logo a resolução efetiva é uns 
4-5 bits menor que a nominal), vide [comparação com ADS1115](https://github.com/bboser/IoT49/blob/master/doc/analog_io.md).
```
import machine
# Vref (internal reference voltage) calibration should be made on each ESP32
machine.ADC.vref()                # read the previous Vref
    (1100, False)
machine.ADC.vref(vref_topin=25)   # route the Vref to GPIO25, which should be measured with a voltmeter (2.039 V ???)
    (1100, 25)
machine.ADC.vref(vref=1121)       # set the Vref to measured value in mV
    (1121, False)
machine.ADC.vref()
    (1121, False)
adc = machine.ADC(machine.Pin(33))
adc
    ADC(Pin(33): unit=ADC1, chan=5, width=12 bits, atten=0dB (1.1V), Vref=1121 mV)
adc.
    __class__       read            ATTN_0DB        ATTN_11DB
    ATTN_2_5DB      ATTN_6DB        HALL            WIDTH_10BIT
    WIDTH_11BIT     WIDTH_12BIT     WIDTH_9BIT      atten
    collect         collected       deinit          progress
    read_timed      readraw         stopcollect     vref
    width
adc.atten(machine.ADC.ATTN_11DB)    # change input attenuation to 11dB (i. e., 0.0-3.9 V)
adc.readraw()
    4095    # 4095 is full scale 
adc.read() 
    3139    # 3139 mV = 3.139 V, measured 3.376 V with voltimeter on GND and 3V3 (at 9 pin female header)
adc.readraw()
    0       # 0 is zero of scale
adc.read()
    142     # 142 mV = 0.142 V, measured 0.000V with voltimeter on GND and GND
adc.width(machine.ADC.WIDTH_10BIT)   # change resolution to 10 bits (0-1023)
    ADC(Pin(36): unit=ADC1, chan=0, width=10 bits, atten=11dB (3.9V), Vref=1114 mV)
adc.readraw()
    1023    # 1023 is full scale 
adc.read() 
    3138    # 3138 mV = 3.138 V, measured 3.376 V with voltimeter on GND and 3V3 (at 9 pin female header)
adc.readraw()
    0       # 0 is zero of scale
adc.read()
    142     # 142 mV = 0.142 V, measured 0.000V with voltimeter on GND and GND
```

- DAC nativo do ESP32 é de 8 bits em 2 pinos (G25 e G26), sendo que o pino G25 está exposto no barramento superior de 9 pinos 
e G26 está no conector Grove do [Atom Matrix](https://docs.m5stack.com/#/en/core/atom_matrix). A documentação oficial de MicroPython 
para ESP32 ainda não cita DAC, dentro do módulo 'machine', mas a [documentação para pyb.DAC](https://docs.micropython.org/en/latest/library/pyb.DAC.html) 
pode ser em parte aproveitada.  
Mas ao que parece UIFlow usou partes de "MicroPython_ESP32_psRAM_LoBo" para implementar mais funções no DAC para ESP32, vide 
[documentação de DAC para MicroPython_ESP32_psRAM_LoBo](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/dac).
```
import machine
dac = machine.DAC(machine.Pin(25))
dac.
    __class__       write           CIRCULAR        NOISE
    NORMAL          RAMP            SAWTOOTH        SINE
    TRIANGLE        beep            deinit          freq
    stopwave        waveform        wavplay         write_buffer
    write_timed
dac
    DAC(Pin(25), channel: 1)
dac.write(0)     # 0.098 V measured with voltimeter on GND & G25
dac.write(64)    # 0.875 V
dac.write(128)   # 1.670 V
dac.write(192)   # 2.449 V
dac.write(255)   # 3.225 V, it should be = 3V3 pin (3.376 V on voltimeter)
```


### 1.3 Consumo de energia

#### 1.3.1 Medição de energia do Atom Matrix usando [UM25C](https://www.aliexpress.com/item/32855845265.html) com cabo USB-C, mas pode estar carregando a bateria :

- 34 mA sem conectar via USB serial/terminal REPL (após 'Ctrl+A+K' no 'screen' para fechar o terminal), 46 mA com terminal REPL;

- 34/46 mA -> 62/74 mA com transmissor IR ligado :
```
from machine import Pin
ir = Pin(12, Pin.OUT)
ir.on()       # IR ligado
```

- 34/46 mA -> 67/78 mA com 'axp.setLcdBrightness(0)', coloca a tela em modo de economia de energia :
```
from m5stack import rgb
rgb.setColor(1, 0xffffff)   # 51.0 mA, com 'brightness' default (30 ?), branco
rgb.setBrightness(0)        # 46.0 mA (apagado)
rgb.setBrightness(10)       # 47.4 mA (branco, brilho fraco)
rgb.setBrightness(30)       # 50.0 mA (branco, brilho fraco a médio)
rgb.setBrightness(64)       # 54.2 mA (branco, brilho médio)
rgb.setBrightness(128)      # 62.44 mA (branco, brilho forte)
rgb.setBrightness(192)      # 70.4 mA (branco, brilho muito forte)
rgb.setBrightness(255)      # 78.3 mA (branco, brilho máximo muito forte)
```

- 34/46 mA -> 1.3/13.1 mA com 'machine.deepsleep(30000)', desliga só o ESP32 (e transmissor IR), ficando RGB LED ligado durante o deepsleep 
(ao reiniciar é apagado), reinicializa em 30s :
```
import machine
machine.deepsleep(30000)
```

Consumo de energia por itens :  
- ESP32 : 32 mA em idle (sem rodar nada); 
- IC USB serial : 1.3-13 mA (sem terminal/com terminal habilitado) ao desligar via 'machine.deepsleep()';
- LED RGB : 5 mA (branco, brilho default fraco a médio), 32 mA (branco, brilho máximo muito forte);
- transmissor IR : 28 mA.



## 2. MicroPython oficial v1.12 ou mais recente

Vide [documentação oficial de MicroPython para ESP32](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html).

É firmware [MicroPython oficial mais recente, v1.12 de 12/2019](http://micropython.org/download), com código-fonte aberto (permite frozen 
modules, adicionar módulos em C, etc). Tem mais (111 kB x 83 kB) RAM livre que UIFlow/MicroPython, menor (44 mA x 46 mA) consumo de energia 
(ainda mais com underclocking, cai até para 22 mA). Usa LittleFS para memória flash interna. Reconhece o LED RGB dentre os poucos drivers 
(apa106, dht, ds18x20 e neopixel). Mas :
* tem que instalar manualmente drivers de terceiros para os vários dispositivos em 'Units'.


### 2.1 Instalando firmware :

Apagando flash, ligar pressionando o 3o botão (C) para entrar em modo 'flash' :  
`$ pip install esptool`  
`$ esptool.py -p /dev/ttyUSB0 -b 115200 --chip esp32 erase_flash`  
Instalando firmware [MicroPython v1.12 ou mais recente, oficial](http://micropython.org/download), sem PSRAM :  
`$ esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash --flash_mode dio -z 0x1000 esp32-idf3-20191220-v1.12.bin`  


### 2.2 Testes e exemplos :

- tem só 1 arquivo em '/pyboard', 'boot.py' (só com comentários e comandos que podem ser habilitados para webrepl, etc), que 
pode ser copiado usando ['rshell'](https://github.com/dhylands/rshell) (mais fácil) ou ['ampy'](https://github.com/scientifichackers/ampy) :
```
$ rshell -p /dev/ttyUSB0
> ls -l /pyboard
       139 Jan  1 2000  boot.py
> cat /pyboard/boot.py
    # This file is executed on every boot (including wake-boot from deepsleep)
    #import esp
    #esp.osdebug(None)
    #import webrepl
    #webrepl.start()
```

- dando reset apertando uma vez o botão laterial :
```
I (458) cpu_start: Pro cpu up.
I (458) cpu_start: Application information:
I (459) cpu_start: Compile time:     Dec 20 2019 07:50:41
I (462) cpu_start: ELF file SHA256:  0000000000000000...
I (468) cpu_start: ESP-IDF:          v3.3
I (472) cpu_start: Starting app cpu, entry point is 0x40083600
I (0) cpu_start: App cpu up.
I (483) heap_init: Initializing. RAM available for dynamic allocation:
I (490) heap_init: At 3FFAE6E0 len 00001920 (6 KiB): DRAM
I (496) heap_init: At 3FFBA488 len 00025B78 (150 KiB): DRAM
I (502) heap_init: At 3FFE0440 len 00003AE0 (14 KiB): D/IRAM
I (508) heap_init: At 3FFE4350 len 0001BCB0 (111 KiB): D/IRAM
I (515) heap_init: At 40092D6C len 0000D294 (52 KiB): IRAM
I (521) cpu_start: Pro cpu start user code
I (204) cpu_start: Chip Revision: 1
W (204) cpu_start: Chip revision is higher than the one configured in menuconfig. Suggest to upgrade it.
I (208) cpu_start: Starting scheduler on PRO CPU.
I (0) cpu_start: Starting scheduler on APP CPU.
MicroPython v1.12 on 2019-12-20; ESP32 module with ESP32
Type "help()" for more information.
>>>
```

- com 'boot.py' de 'MicroPython v1.12 on 2019-12-20' :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.12 on 2019-12-20; ESP32 module with ESP32
import gc
gc.collect()
gc.mem_free()
    114368   # 111.7 kB
```

- módulo 'os' em [MicroPython >= v1.12 suporta LittleFS via função 'VfsLfs2()'](https://docs.micropython.org/en/latest/library/uos.html#uos.VfsLfs2), 
mas a flash interna vem formatada com FAT (ao menos até MicroPython <= 1.12), 
[vide no tópico 'littlefs - advantages & disadvantages' do fórum Micropython](https://forum.micropython.org/viewtopic.php?f=2&t=7228&start=10#p41917) 
um teste simples. Então [formatamos para LittleFS](https://docs.micropython.org/en/latest/reference/filesystem.html?highlight=littlefs#littlefs) :
```
import os
os.
    VfsLfs2         chdir           dupterm         dupterm_notify
    getcwd          ilistdir        listdir         mkdir
    mount           rename          rmdir           stat
    statvfs         umount          uname           urandom
os.uname()   # versão de MicroPython
    (sysname='esp32', nodename='esp32', release='1.12.0', version='v1.12 on 2019-12-20', machine='ESP32 module with ESP32')
# Testando se a flash interna usa FAT ou LittleFS
import flashbdev
buf = bytearray(8)
flashbdev.bdev.readblocks(0, buf)
buf
    bytearray(b'\xeb\xfe\x90MSDOS')   # Usa FAT como default ao ter o firmware instalado
# Formatando com LittleFS :
os.umount('/')
os.VfsLfs2.mkfs(bdev)
os.mount(bdev, '/')
# Teste não mais mostra 'MSDOS' ao final, pois foi instalado LittleFS como sistema de arquivos
flashbdev.bdev.readblocks(0, buf)
buf
    bytearray(b'\xf9\xff\xff\xff\xf0\x0f\xff\xf7')    
```

- [módulo 'machine'](https://docs.micropython.org/en/latest/library/machine.html) e frequência do ESP32 (default é 160 MHz, 
pode ser alterada para 20MHz, 40MHz, 80Mhz, 160MHz or 240MHz) :
```
import machine
machine.freq()
    160000000
machine.freq(240000000)
machine.freq(160000000)
machine.freq(80000000)
machine.freq(40000000)
machine.freq(20000000)
```

- help :
```
help()
    Welcome to MicroPython on the ESP32!
    
    For generic online docs please visit http://docs.micropython.org/
    
    For access to the hardware use the 'machine' module:
    
    import machine
    pin12 = machine.Pin(12, machine.Pin.OUT)
    pin12.value(1)
    pin13 = machine.Pin(13, machine.Pin.IN, machine.Pin.PULL_UP)
    print(pin13.value())
    i2c = machine.I2C(scl=machine.Pin(21), sda=machine.Pin(22))
    i2c.scan()
    i2c.writeto(addr, b'1234')
    i2c.readfrom(addr, 4)
    
    Basic WiFi configuration:
    
    import network
    sta_if = network.WLAN(network.STA_IF); sta_if.active(True)
    sta_if.scan()                             # Scan for available access points
    sta_if.connect("<AP_name>", "<password>") # Connect to an AP
    sta_if.isconnected()                      # Check for successful connection
    
    Control commands:
      CTRL-A        -- on a blank line, enter raw REPL mode
      CTRL-B        -- on a blank line, enter normal REPL mode
      CTRL-C        -- interrupt a running program
      CTRL-D        -- on a blank line, do a soft reset of the board
      CTRL-E        -- on a blank line, enter paste mode
    
    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (drivers, etc, do UIFlow MicroPython firmware) :

```
help('modules')
    __main__          framebuf          ucryptolib        urandom
    _boot             gc                uctypes           ure
    _onewire          inisetup          uerrno            urequests
    _thread           machine           uhashlib          uselect
    _webrepl          math              uhashlib          usocket
    apa106            micropython       uheapq            ussl
    btree             neopixel          uio               ustruct
    builtins          network           ujson             utime
    cmath             ntptime           umqtt/robust      utimeq
    dht               onewire           umqtt/simple      uwebsocket
    ds18x20           sys               uos               uzlib
    esp               uarray            upip              webrepl
    esp32             ubinascii         upip_utarfile     webrepl_setup
    flashbdev         ucollections      upysh             websocket_helper
    Plus any modules on the filesystem
```

- [módulo 'esp'](https://docs.micropython.org/en/latest/library/esp.html) e tamanho da memória flash interna (4 MB) :
```
import esp
esp.
    __class__       __name__        LOG_DEBUG       LOG_ERROR
    LOG_INFO        LOG_NONE        LOG_VERBOSE     LOG_WARNING
    dht_readinto    flash_erase     flash_read      flash_size
    flash_user_start                flash_write     gpio_matrix_in
    gpio_matrix_out                 neopixel_write  osdebug
esp.flash_size()
    4194304   # 4MB
```

- [módulo 'esp32'](https://docs.micropython.org/en/latest/library/esp32.html), temperatura e leitura do sensor Hall do ESP32 :
```
import esp32
esp32.
    __class__       __name__        ULP             WAKEUP_ALL_LOW
    WAKEUP_ANY_HIGH                 hall_sensor     raw_temperature
    wake_on_ext0    wake_on_ext1    wake_on_touch
esp32.raw_temperature()   # in F
    123
(esp32.raw_temperature() - 32)/1.8   # in C
    50.55556
esp32.hall_sensor()
    4
```

- LED RGB via [driver neopixel de MicroPython oficial para ESP32](https://docs.micropython.org/en/latest/esp32/quickref.html#neopixel-driver), 
está conectado ao pino G27, [vide documentação do Atom Matrix](https://docs.m5stack.com/#/en/core/atom_matrix) :
```
from machine import Pin
import neopixel
neopixel.
    __class__       __name__        __file__        neopixel_write
    NeoPixel
np = neopixel.NeoPixel(Pin(27, Pin.OUT), 1)   # create NeoPixel driver on GPIO27 for 1 pixel
np.
    __class__       __getitem__     __init__        __module__
    __qualname__    __setitem__     write           __dict__
    fill            pin             ORDER           n
    bpp             buf             timing
np[0]                   # get 1st pixel colour
    (0, 0, 0)           # black (off)
np[0] = (255, 255, 255) # set the 1st pixel to white, maximum brightness
np.write()              # write data to all pixels
np.fill((128,0,0))      # all pixels to red, medium brightness
np.write()              # write data to all pixels
np[0]         # get first pixel colour
```

- botão A está conectado ao pino G39, [vide documentação do Atom Matrix](https://docs.m5stack.com/#/en/core/atom_matrix) :
```
from machine import Pin
btnA = Pin(39, Pin.IN)
btnA.
    __class__       value           IN              IRQ_FALLING
    IRQ_RISING      OPEN_DRAIN      OUT             PULL_DOWN
    PULL_HOLD       PULL_UP         WAKE_HIGH       WAKE_LOW
    init            irq             off             on
btnA.value()    # sem pressionar o botão frontal, botão A
    1
btnA.value()    # pressionando o botão frontal, botão A 
    0
```

- transmissor IR está conectado ao pino G12 (não tem diagrama, mas parece que alimenta na parte positiva do LED IR), 
[vide documentação do Atom Matrix](https://docs.m5stack.com/#/en/core/atom_matrix) :
```
from machine import Pin
ir = Pin(12, Pin.OUT)
ir.
    __class__       value           IN              INOUT
    IRQ_FALLING     IRQ_RISING      OPEN_DRAIN      OUT
    OUT_OD          PULL_DOWN       PULL_FLOAT      PULL_HOLD
    PULL_UP         WAKE_HIGH       WAKE_LOW        deinit
    init            irq             off             on
ir.value()    # IR desligado
    0
ir.value(1)   # IR ligado
ir.off()      # IR desligado
ir.on()       # IR ligado
```

- RTC nativo de MicroPython oficial para ESP32, vide [exemplo](http://docs.micropython.org/en/v1.10/esp32/quickref.html#real-time-clock-rtc), 
sendo que não segue o padarão de [RTC do MicroPython oficial](https://docs.micropython.org/en/latest/library/machine.RTC.html), não tendo 
alarme, etc.  
'RTC.datetime()' precisa ter 8 argumentos que não são bem documentados, mas vide essa 
[GitHub issue 'RTC cannot be set correctly on ESP 32 #4540'](https://github.com/micropython/micropython/issues/4540#issuecomment-580965520),
notando que dia da semana (0-6 começando com 2a-feira) é desprezado pois é calculado automaticamente.  
**O RTC nativo do ESP32 não mantém hora e data se o ESP32 for desligado** (apertando o botão lateral para desligar o Atom Matrix).
```
from machine import RTC
rtc = RTC()
rtc.
    __class__       datetime        init            memory
rtc.datetime()
    (2000, 1, 1, 5, 0, 15, 23, 883318)
rtc.datetime((2020, 3, 25, 0, 15, 43, 50, 0))   # (year, month, day, day of week, hour, minute, second, microsecond, tzinfo)
```

- I2C via ['machine.I2C'](https://docs.micropython.org/en/latest/esp32/quickref.html#i2c-bus), forma padrão do MicroPython para ESP32. 
Com barramento I2C listando nenhum dispositivo :
```
from machine import I2C, Pin
i2c = I2C(freq=400000, sda=Pin(25), scl=Pin(21))
i2c.
    __class__       readinto        start           stop
    write           init            readfrom        readfrom_into
    readfrom_mem    readfrom_mem_into               scan
    writeto         writeto_mem     writevto
i2c.scan()
    []   # no I2C devices
```

<img src="https://docs.m5stack.com/assets/img/product_pics/unit/tof/unit_tof_01.jpg" alt="M5Stack Unit ToF VL53L0X" width="300"/>

- [Unit TOF VL53L0X](https://docs.m5stack.com/#/en/unit/tof). O barramento I2C no conector Grove do Atom Matrix usa sda=G26 e scl=G32, 
[vide documentação do Atom Matrix](https://docs.m5stack.com/#/en/core/atom_matrix).  
O [driver MicroPython para VL53L0X de Radomir Dopieralski](https://bitbucket.org/thesheep/micropython-vl53l0x/src/default/) é mais 
completo que o driver UIFlow/MicroPython em termos de acesso aos recursos do 
['VL53L0X Time-of-Flight (ToF) ranging sensor'](https://www.st.com/en/imaging-and-photonics-solutions/vl53l0x.html), por exemplo :
  1. range profile 'default mode' : maximum rane 120 cm, range timing budget 30 ms, accuracy < 4%;
  2. range profile 'high accuracy' : maximum rane 120 cm, range timing budget 200 ms, accuracy < 3%;
  3. range profile 'long range' : maximum rane 200 cm, range timing budget 33 ms, accuracy < 4%;
  4. range profile 'high speed' : maximum rane 120 cm, range timing budget 20 ms, accuracy < 5%;
  5. 'single ranging' mode : leitura só quando função é chamada;
  6. 'continuous ranging' mode : leitura em modo contínuo, lendo novamente logo que leitura anterior acaba;
  7. 'continuous timed ranging' mode : leitura em modo contínuo, lendo novamente um intervalo de tempo após leitura anterior acabar;  

O driver adota possivelmente (1) como default (não é configurável); (5) via função 'read()'; (6-7) via 'start(period=0)' (('period' é 
em ms mas parece que tem um bug quando diferente de 0 resultando em 'TimeoutError:') e 'stop()' com leitura via 'read()' consomido 20 mA.
```
import time
import vl53l0x
from machine import I2C, Pin
i2c = I2C(freq=400000, sda=Pin(26), scl=Pin(32))
i2c.scan()
    [41]        #  VL53L0X default I2C address is 0x29 = 41
vl53l0x.
    __class__       __name__        const           __file__
    TimeoutError    ustruct         utime           _IO_TIMEOUT
    VL53L0X
tof = vl53l0x.VL53L0X(i2c)
tof.
    __class__       __init__        __module__      __qualname__
    read            start           stop            __dict__
    init            i2c             address         _started
    _registers      _register       _flag           _config
    _stop_variable  _spad_info      _calibrate
while True:
    print(tof.read())
    time.sleep_ms(200)
# Usando 'single ranging' mode (default), 'read()' espera pela medida ser feita
while True:
    ti = time.ticks_us()
    d = tof.read()
    tf = time.ticks_us()
    dt = (tf - ti)
    print('d = {} mm, dt = {} ms'.format(d, dt/1000))   # d = 93 mm, dt = 35.848 ms
# Usando 'continuous ranging' mode, as medidas estão sendo feitas continuamente em background pelo sensor, se a medida já estiver 
# feita então 'read()' não espera :
tof.start()
while True:
    ti = time.ticks_us()
    d = tof.read()
    tf = time.ticks_us()
    dt = (tf - ti)
    print('d = {} mm, dt = {} ms'.format(d, dt/1000))   # d = 96 mm, dt = 2.256 ms
    time.sleep_ms(35)
```

- ADC nativo do ESP32 é de 12 bits, em MicroPython limitado aos pinos 32 a 39, sendo que no [Atom Matrix](https://docs.m5stack.com/#/en/core/atom_matrix)
o pino G33 está exposto no barramento superior de 9 pinos e o pino G32 está no conector Grove.  
Vide [documentação oficial de MicroPython para ESP32 usando ADC](https://docs.micropython.org/en/latest/esp32/quickref.html#adc-analog-to-digital-conversion).  
Notar que **o ADC do ESP32 tem diversos problemas** : offset (60-130 mV), não-linearidade e alto ruído (logo a resolução efetiva é uns 
4-5 bits menor que a nominal), vide [comparação com ADS1115](https://github.com/bboser/IoT49/blob/master/doc/analog_io.md).
```
import machine
machine.ADC.
    __class__       __name__        read            __bases__
    ATTN_0DB        ATTN_11DB       ATTN_2_5DB      ATTN_6DB
    WIDTH_10BIT     WIDTH_11BIT     WIDTH_12BIT     WIDTH_9BIT
    atten           read_u16        width
adc = machine.ADC(machine.Pin(33))
adc
    ADC(Pin(33))
adc.
    __class__       read            ATTN_0DB        ATTN_11DB
    ATTN_2_5DB      ATTN_6DB        WIDTH_10BIT     WIDTH_11BIT
    WIDTH_12BIT     WIDTH_9BIT      atten           read_u16
    width
adc.atten(machine.ADC.ATTN_11DB)    # change input attenuation to 11dB (i. e., 0.0-3.9 V)
adc.read()
    4095    # 4095 is full scale, connected to 3V3 
adc.read()
    0       # 0 is zero of scale, connected to GND
adc.width(machine.ADC.WIDTH_10BIT)   # change resolution to 10 bits (0-1023)
adc.read()
    1023    # 1023 is full scale, connected to 3V3  
adc.read()
    0       # 0 is zero of scale, connected to GND
```

- DAC nativo do ESP32 é de 8 bits em 2 pinos (G25 e G26), sendo que o pino G25 está exposto no barramento superior de 9 pinos e G26 está no 
conector Grove do [Atom Matrix](https://docs.m5stack.com/#/en/core/atom_matrix). A documentação oficial de MicroPython para ESP32 ainda não cita DAC, 
dentro do módulo 'machine', mas a [documentação para pyb.DAC](https://docs.micropython.org/en/latest/library/pyb.DAC.html) pode ser em parte 
aproveitada, i. e., só comando 'write()'.  
```
import machine
dac = machine.DAC(machine.Pin(25))
dac.
    __class__       write
dac
    DAC(Pin(25), channel: 1)
dac.write(0)     # 0.099 V measured with voltimeter on GND & G25
dac.write(64)    # 0.875 V
dac.write(128)   # 1.670 V
dac.write(192)   # 2.449 V
dac.write(255)   # 3.225 V, it should be = 3V3 pin (3.379 V on voltimeter)
```


### 2.3 Consumo de energia

#### 2.3.1 Medição de energia do Atom Matrix usando [UM25C](https://www.aliexpress.com/item/32855845265.html) com cabo USB-C, mas pode estar carregando a bateria :

- 24 mA sem conectar via USB serial/terminal REPL (após 'Ctrl+A+K' no 'screen' para fechar o terminal), 36 mA com terminal REPL;

- 10-32/22-44 mA variando frequência do ESP32 entre 20 MHz, 40 MHz, 80 MHz, 160 MHz e 240 MHz :
```
import machine
machine.freq(240000000)   # 32/44 mA
machine.freq(160000000)   # default, 24/36 mA
machine.freq(80000000)    # 19/31 mA
machine.freq(40000000)    # 12/24 mA
machine.freq(20000000)    # 10/22 mA
```

- 24/36 mA -> 52/63 mA com transmissor IR ligado :
```
from machine import Pin
ir = Pin(12, Pin.OUT)
ir.on()       # IR ligado
```

- 24/36 mA -> 56/68 mA com 'axp.setLcdBrightness(0)', coloca a tela em modo de economia de energia :
```
from machine import Pin
import neopixel
np = neopixel.NeoPixel(Pin(27, Pin.OUT), 1)   # create NeoPixel driver on GPIO27 for 1 pixel
np[0] = (10, 10, 10)        # 36.8 mA (branco, brilho fraco)
np.write()
np[0] = (30, 30, 30)        # 39.2 mA (branco, fraco a médio)
np.write()
np[0] = (64, 64, 64)        # 43.6 mA (branco, brilho médio)
np.write()
np[0] = (128, 128, 128)     # 51.6 mA (branco, brilho forte)
np.write()
np[0] = (192, 192, 192)     # 59.7 mA (branco, brilho muito forte)
np.write()
np[0] = (255, 255, 255)     # 67.8 mA (branco, brilho máximo muito forte)
np.write()
np[0] = (0, 0, 0)           # 35.5 mA (apagado)
np.write()
```

- 24/36 mA -> 1.3/13.1 mA com 'machine.deepsleep(30000)', desliga só o ESP32 (e transmissor IR), ficando RGB LED ligado durante o deepsleep 
e depois de reinicia, reinicializa em 30s :
```
import machine
machine.deepsleep(30000)
```

Consumo de energia por itens :  
- ESP32 : 9/11/18/23/31 mA em idle (sem rodar nada) @ 20/40/80/160/240 MHz;
- IC USB serial : 1.3-13 mA (sem terminal/com terminal habilitado) ao desligar via 'machine.deepsleep()';
- LED RGB : 3.7 mA (branco, brilho fraco a médio), 32 mA (branco, brilho máximo muito forte);
- transmissor IR : 28 mA.
