# Tutorial sobre Ferramentas de Programação MicroPython para BBC Micro:bit

Recomendações :

Sobre [conexão de BBC Micro:bit a computador](https://support.microbit.org/support/solutions/articles/19000013984-how-do-i-connect-the-micro-bit-to-my-computer). No Linux, 'usuario' precisa ter permissão no grupo 'dialout' para poder acessar dispositivo BBC Micro:bit montado em '/dev/ttyACM0'.  
Por exemplo, em Debian e variantes :  
`$ sudo usermod -a -G dialout usuario`

MicroPython se diferencia da maioria das linguagens de microcontroladores por ser interpretada, permitindo interatividade elevada. 
Logo a sugestão é usar ferramentas de programação que incluam modo interativo (REPL).

Escolha das melhores ferramentas abaixo :

- online : EduBlocks se preferir programação em blocos, senão Python Editor v2 (oficial) que suporta WebUSB (REPL, etc);
- offline : Mu Editor, Thonny;
- online & simulação : create.withcode.uk.


# Editores de Programação MicroPython para BBC Micro:bit

Há diversos editores MicroPython online e offline para BBC Micro:bit :


## EduBlocks Editor : https://app.edublocks.org/

Online, tem modo para BBC Micro:bit (selecione ao iniciar), permite **programação usando blocos**. 
[Recomendo o tutorial da Kitronik.](https://www.kitronik.co.uk/blog/getting-started-edublocks-microbit/)

<img src="https://1.bp.blogspot.com/-eyWkXGg9hGY/W3p6jKPfcxI/AAAAAAAAEE4/QXDkYy9NGeA6il8DGdXc7PSkWbQ34qtSwCEwYBhgL/s1600/Screen%2BShot%2B2018-08-20%2Bat%2B09.22.42.png" alt="EduBlocks em ação" width="600"/>

Dicas :

* no canto esquerdo estão as categorias de blocos, basta arrastar e ir encaixando os blocos;
* no canto direito superior tem botão "Blocks" e "Python" para alternar os modos de exibição de blocos e código MicroPython direto;
* faça transferência do arquivo .hex (firmware gerado) para o Micro:bit via botão "Download Hex", depois arraste (usando gerenciador 
  de arquivos) para o BBC Micro:bit, mostrado como (falso) pendrive "MICROBIT";
* em "Settings" tem botão "Export Python" para exportar o código MicroPython em arquivo .py;
* em "Settings" tem botão "Flash Hex" para gravar o arquivo .hex (firmware gerado) no Micro:bit.


## BBC Micro:bit Python Editor (oficial) : https://python.microbit.org

Online, está na versão 1.1, com algumas poucas mudanças em relação à primeiro versão.

<img src="https://microbit.org/images/python-editor.png" alt="Python Editor v1.1 para BBC Micro:bit" width="600"/>

Dicas :

* faça download do arquivo .hex (firmware gerado) no Micro:bit via botão "Download" no seu computador, depois arraste 
(usando gerenciador de arquivos) para o BBC Micro:bit, mostrado como (falso) pendrive "MICROBIT";
* aproveite o botão "Snippets" para inserir códigos mais usados em Python;
* use e abuse do botão "Help" que tem link para [documentação de MicroPython para Micro:bit](https://microbit-micropython.readthedocs.io), 
[documentação do Python Editor para Micro:bit](https://python-editor-1-1-5.microbit.org/help.html), 
bem como [documentação geral sobre Micro:bit](https://support.microbit.org/support/home);

Recomendo clicar no topo em "Python Editor beta" para acessar a prévia da 
[nova versão 2](https://support.microbit.org/support/solutions/articles/19000102970-python-editor-version-2-update) com mais recursos : 
https://python.microbit.org/v/beta
<img src="https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/19048566395/original/4sXyjuBIiVRlLVOu9wz2qK7dTg_59siXsw.png" 
alt="Python Editor v2 para BBC Micro:bit" width="600"/>

Tal Python Editor v2 tem WebUSB, que deve ser compatível com o navegador web (Chrome e derivados) e permissões do sistema operacional, para que a 
conexão ao Micro:bit seja feita.

Dicas :

* **'auto completar'** já vem habilitado (vide botão "Beta Options"), além de ter **TAB para completar manualmente**;
* botão "Connect", se WebUSB funcionar, transforma o botão "Download" em "Flash", sem precisar usar gerenciador de arquivos;
* botão "Load/Save" permite ler e salvar scripts .py ou firmwares .hex, e em "Project Files" permite copiar para e do 
 Micro:bit arquivos quaisquer (até uns 30 kB somente);
* botão **"Open Serial" dá acesso ao modo interativo (REPL)** do MicroPython no Micro:bit, se WebUSB funcionar;
* use e abuse do botão "Help" que tem link para [documentação de MicroPython para Micro:bit](https://microbit-micropython.readthedocs.io), 
[documentação do Python Editor v2 para Micro:bit](https://python-editor-2-0-0-beta-4.microbit.org/help.html), 
bem como [documentação geral sobre Micro:bit](https://support.microbit.org/support/home).


## Mu Editor : https://codewith.mu/
<img src="https://codewith.mu/img/en/tutorials/mu_buttons.png" alt="Menu do Mu Editor" width="600"/>

Offline, o melhor editor MicroPython para BBC Micro:bit em termos de recursos diversos :  

* versões para [Linux, Mac OS, Raspbian, Windows](https://codewith.mu/en/download).
* em português;
* auto completar simples;
* [modo interativo (REPL)](https://codewith.mu/en/tutorials/1.0/repl) opcional abaixo do modo editor;
* modo para transferência opcional permite copiar para e do Micro:bit arquivos quaisquer (até uns 30 kB somente);
* [modo gráfico de linha(s)](https://codewith.mu/en/tutorials/1.0/plotter) em tempo real a partir 'print' de tupla;
* verificação de código;
* [atalhos de teclado](https://codewith.mu/en/tutorials/1.0/shortcuts);
* documentação muito boa em termos de [tutoriais](https://codewith.mu/en/tutorials/) e [how-to's](https://codewith.mu/en/howto/);

Desvantagem é a dificuldade de instalar em Linux (fora o Raspbian), que frequentemente resulta em problemas de dependências.


## create.withcode.uk : https://create.withcode.uk/

Online. create.withcode.uk é editor e simulador MicroPythonand para BBC Micro:bit. 

[Tutorial e alguns exemplos](https://community.computingatschool.org.uk/resources/4479/single).

Tem poucos recursos de edição para BBC Micro:bit :

* execução de scripts .py no simulador de BBC Micro:bit;
* gera firmware .hex para gravar no BBC Micro:bit;
* simula uso de tela de 5x5 LEDs, 2 botões, sensores, radio, etc.


## Atom Editor : https://atom.io/ 

Offline. Atom é um IDE (Integrated Development Environment) multiplaforma (Linux, Mac OS, Windows), gratuito e de código-fonte aberto.

Com [plugin MicroPython para Micro:bit](https://atom.io/packages/microbit-micropython), porém só para Linux.

Tem recursos básicos para BBC Micro:bit :

* gera firmware .hex e grava no BBC Micro:bit;
* execução de scripts .py no BBC Micro:bit;
* modo interativo (REPL).

## PyCharm : https://www.jetbrains.com/pycharm/

Offline. PyCharm é um IDE multiplaforma (Linux, Mac OS, Windows), com versão gratuita e comercial.

Usando email institucional da UFES (p. e., login_no_portal_do_aluno@aluno.ufes.br), crie uma conta no site [JetBrains](https://www.jetbrains.com/] (canto superior direito). Depois clique em "Apply for a free student or teacher license for 
educational purposes" para se registrar em qualquer software comercial da JetBrains, incluindo PyCharm.

Depois instale o [plugin MicroPython](https://github.com/vlasovskikh/intellij-micropython), [anunciado em 2018](https://blog.jetbrains.com/pycharm/2018/01/micropython-plugin-for-pycharm/), 
compatível com ESP8266, Pyboard e BBC Micro:bit.

Tem os recursos mais importantes :

* auto completar e documentação de código;
* verificação de código;
* modo interativo (REPL);
* transferência e execução de scripts .py no microcontrolador.


## Thonny : https://thonny.org/

<img src="https://bitbucket-assetroot.s3.amazonaws.com/repository/yppMM48/303187989-Variables.PNG?AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ&Expires=1570480135&Signature=QFL%2BBOlgW7%2FzLGPHd1Yj9JQ8MRk%3D" 
alt="Thonny com BBC Micro:bit" width="600"/>

Offline. Thonny é um IDE para iniciantes em Python, multiplaforma (Linux, Mac OS, Windows), gratuito e de código-fonte aberto.

Depois instale o [plugin MicroPython para BBC Micro:bit](https://bitbucket.org/KauriRaba/thonny-microbit/), vide 
[instruções](https://bitbucket.org/KauriRaba/thonny-microbit/wiki/installation-guide).

Recursos bem amplos :

* auto completar de código;
* debug de variáveis;
* gera firmware .hex e grava no BBC Micro:bit;
* execução de scripts .py no BBC Micro:bit;
* modo interativo (REPL).


## Visual Studio Code : https://code.visualstudio.com/

Offline. VS Code é um IDE multiplaforma (Linux, Mac OS, Windows), gratuito e de código-fonte aberto.

Com [extensão MicroPython para Micro:bit](https://marketplace.visualstudio.com/items?itemName=PhonicCanine.micro-bit).

Tem recursos limitados para BBC Micro:bit :

* auto completar de código;
* gera firmware .hex e grava no BBC Micro:bit.


# Ferramentas Comando-de-Linha (CLI) de Programação MicroPython em BBC Micro:bit

Vide [documentação sobre exibição de dados seriais do Micro:bit em computador via cabo USB serial](https://support.microbit.org/support/solutions/articles/19000022103-outputing-serial-data-from-the-micro-bit-to-a-computer).

## screen (Linux / Mac OS)

Para acesso interativo (REPL - Read Evaluate Print Loop). Usar no terminal Linux :  
`$ screen /dev/ttyACM0 115200`  
Digite enter para aparecer o prompt '>>>', ou 'Ctrl+C' para interromper algum programa rodando.
para fechar a sessão do screen no REPL, digite 'Ctrl+A' seguido de 'D'.


## [microfs/ufs](https://microfs.readthedocs.io/en/latest/) (Linux / Mac OS / Windows)

microfs/ufs é uma ferramenta CLI para listar, copiar e apagar arquivos do sistema de arquivos da memória flash do BBC Micro:bit.

Mais exemplos na [documentação oficial de MicroPython para BBC Micro:bit](https://microbit-micropython.readthedocs.io/en/latest/tutorials/storage.html#file-transfer).


## [uflash](https://uflash.readthedocs.io/) (Linux / Mac OS / Windows)

uflash é uma ferramenta CLI para gravar e ler firmware .hex, com ou sem script MicroPython que rodará automaticamente quando o Micro:bit ligar/reiniciar.
