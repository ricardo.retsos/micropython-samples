# Firmwares for MicroPython on Pyboard v1.1/Lite v1.0/D SF2/3/6

All MicroPython firmwares were compiled from the [MicroPython source code](https://github.com/micropython/micropython) by me, without any warranty.

Look at the [MicroPython Download site](http://micropython.org/download) for official firmwares for all types of Pyboard's and other MicroPython boards.

The firmware name is of the form :  
```<Pyboard name>-<optional 'dp' and/or 'thread'>-<version with date>.dfu```  
where :
- 'dp' means double precision (FP64) for float point numbers, the default is single precision (FP32) 
on almost all microcontrollers (except Pyboard D SF6W on the [MicroPython Download site](http://micropython.org/download));
- 'thread' means builds containing the _thread module and allowing multithreading.

For example :  
```PYBD-SF6-dp-thread-v1.11-499-gece4e21a5_20191028.dfu```  
means it is a v1.11-499 firmware, from October 28th 2019, with double precision and threads enabled, for Pyboard D SF6.